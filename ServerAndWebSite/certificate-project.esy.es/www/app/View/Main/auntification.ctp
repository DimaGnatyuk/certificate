<div class="login-page bk-img" style="background-image: url(/img/login-bg.jpg);">
	<div class="form-content">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<h1 class="text-center text-bold text-light mt-4x">Sign in</h1>
					<div class="well row pt-2x pb-3x bk-light">
						<div class="col-md-8 col-md-offset-2">
							<form action="/main/login" method="POST" class="mt">

								<label for="" class="text-uppercase text-sm">Your Email</label>
								<input name="email" type="text" placeholder="Username" class="form-control mb">

								<label for="" class="text-uppercase text-sm">Password</label>
								<input name="password" type="password" placeholder="Password" class="form-control mb">

								<button class="btn btn-success btn-block" type="submit">LOGIN</button>
								<a class="btn btn-primary btn-block" data-toggle="modal" data-target="#registrationModalWindows">REGISTRATION</a>
							</form>
						</div>
					</div>
					<div class="text-center text-light">
						<a href="#" class="text-light">Forgot password?</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Registration Modal -->
<div id="registrationModalWindows" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <form method="post" action="/main/registration">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Registration</h4>
      </div>
      <div class="modal-body">
        <div class="control-group">
			<label class="control-label"  for="name">Username</label>
			<input type="text" name="name" required placeholder="" class="form-control">
		</div>
		<div class="control-group">
			<label class="control-label"  for="username">Email</label>
			<input type="email" name="email" required placeholder="" class="form-control">
		</div>
		<div class="control-group">
			<label class="control-label" required  for="username">Password</label>
			<input type="password" name="password1" placeholder="" class="form-control">
		</div>
		<div class="control-group">
			<label class="control-label" required for="username">Password</label>
			<input type="password" name="password2" placeholder="" class="form-control">
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">Registration</button>
      </div>
        </form>
    </div>

  </div>
</div>