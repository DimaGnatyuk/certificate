<div class="brand clearfix">
		<a href="/main/" class="logo"><img src="/img/logo.jpg" class="img-responsive" alt=""></a>
		<span class="menu-btn"><i class="fa fa-bars"></i></span>
		<ul class="ts-profile-nav">
			<li><a href="/main/help">Help</a></li>
			<li class="ts-account">
				<a href="#"><img src="/img/ts-avatar.jpg" class="ts-avatar hidden-side" alt=""> Account <i class="fa fa-angle-down hidden-side"></i></a>
				<ul>
					<li><a href="#">My Account</a></li>
					<li><a href="/main/config">Edit Account</a></li>
					<li><a href="/main/logout">Logout</a></li>
				</ul>
			</li>
		</ul>
	</div>

	<div class="ts-main-content">
		<nav class="ts-sidebar">
			<ul class="ts-sidebar-menu">
				<li class="ts-label">Main</li>
				<li><a href="/main/"><i class="fa fa-dashboard"></i> Головна</a></li>
				<li><a href="/main/viewAddRoom"><i class="fa fa-desktop"></i> Add Room</a></li>
				<li><a href="#"><i class="fa fa-files-o"></i> Download App</a>
					<ul>
						<li><a href="blank.html">PC Client</a></li>
						<li><a href="login.html">Android Client</a></li>
						<li><a href="login.html">App Server</a></li>
						<li><a href="login.html">Dump DataBase</a></li>
					</ul>
				</li>

				<!-- Account from above -->
				<ul class="ts-profile-nav">
					<li><a href="#">Help</a></li>
					<li><a href="#">Settings</a></li>
					<li class="ts-account">
						<a href="#"><img src="img/ts-avatar.jpg" class="ts-avatar hidden-side" alt=""> Account <i class="fa fa-angle-down hidden-side"></i></a>
						<ul>
							<li><a href="#">My Account</a></li>
							<li><a href="#">Edit Account</a></li>
							<li><a href="#">Logout</a></li>
						</ul>
					</li>
				</ul>

			</ul>
		</nav>
		<div class="content-wrapper">
			<div class="container-fluid">

				<div class="row">
					<div class="col-md-12">

						<h2 class="page-title">Всі приміщення</h2>
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
							<div class="panel-body">
								<table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>id</th>
											<th>Room</th>
											<th>Last action</th>
											<th>Data</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th>id</th>
											<th>Room</th>
											<th>Last action</th>
											<th>Data</th>
										</tr>
									</tfoot>
									<tbody>
										<?php foreach($rooms as $root) :?>
										<tr class="<?php echo isset($root["LastAction"])?$root["LastAction"]["State"]["action"]:""?>">
											<td><?php echo $root["id"]?></td>
											<td><a href="/main/viewRoom/<?php echo $root["id"]?>"><?php echo $root["name"]?></a></td>
											<td><?php echo isset($root["LastAction"])?$root["LastAction"]["State"]["name"]:"---"?></td>
											<td><?php echo isset($root["LastAction"])?$root["LastAction"]["Report"]["data"]:"---"?></td>
										</tr>
									<?php endforeach;?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="clearfix pt pb">
						<div class="col-md-12">
							<em>Thank you for using <a href="http://themestruck.com/theme/harmony/"> Harmony Admin Theme </a> by <a href="http://themestruck.com/">ThemeStruck</a></em>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>