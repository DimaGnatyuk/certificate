<?php 
	class ApiController extends AppController {

		private function rend ($response){
			$this->set(compact("response"));
			$this->render("api");
		}

		private function hasMd ($login, $password){
			return md5("$login+lol+$password");
		}

		public function __construct($request = null, $response = null)
    	{
        	parent::__construct($request, $response);
        	$this->layout="api";
    	}

    	public function index (){
    		$responce = array("error" => 0,"data" => "Api 1.0");
    		$this->rend($responce);
    	}

    	public function auntification (){
    		$responce = array("error" => 0,"data" => "");
			try{
				if (!isset($_POST["email"])){
					throw new Exception("email - null");
				}
				if (!isset($_POST["password"])){
					throw new Exception("password - null");
				}
				$email = $_POST["email"];
				//$login = $_POST["login"] = "testlogin@com.ua";
				$password = $_POST["password"];
				//$password = $_POST["password"] = "123456789";
				$this->loadModel("People");
				$people = $this->People->find("first",array("conditions" => array("People.has_md"=>$this->hasMd($email,$password))));
				if ($people == null)
				{
					throw new Exception ("User not found");
				}

				if(isset($people["Room"])){
					unset($people["Room"]);
				}

				$responce["data"] = $people;
			}catch(Exception $error){
				$responce["error"] = 1;
				$responce["data"] = $error->getMessage();
			}
			$this->rend($responce);
		}

		public function getAllRooms ($token = ""){
			$responce = array("error" => 0,"data" => "");
			try{
				if ($token == ""){
					throw new Exception ("token not found");
				}
				$this->loadModel("People");
				$user = $this->People->find("first",array("conditions"=>array("People.has_md" => $token)));
				if ($user == null){
					throw new Exception ("User not found");
				}

			if ($user){
				for ($i=0;$i<count($user["Room"]);$i++){
					$this->loadModel("Report");
					$action = $this->Report->find("first", array("conditions" => array("Report.room_id" => $user["Room"][$i]["id"]),"order" => array('Report.id DESC')));
					if ($action != null){
						$user["Room"][$i]["LastAction"] = $action;
					}else{
						$user["Room"][$i]["LastAction"] = "none";
					}
				}
			}
			$responce["data"] = $user["Room"];
			}catch(Exception $error){
				$responce["error"] = 1;
				$responce["data"] = $error->getMessage();
			}
			$this->rend($responce);
		}

		public function getTestStatus ($token = "",$has_md = ""){
			$responce = array("error" => 0,"data" => "");
			try{
				if ($token == ""){
					throw new Exception ("token not found");
				}
				$this->loadModel("People");
				$user = $this->People->find("first",array("conditions"=>array("People.has_md" => $token)));
				if ($user == null){
					throw new Exception ("User not found");
				}
				$a = array();
				if ($user){
					for ($i=0;$i<count($user["Room"]);$i++){
						$this->loadModel("Report");
						$action = $this->Report->find("first", array("conditions" => array("Report.room_id" => $user["Room"][$i]["id"],"Report.state_id"=>array(-1, -3)),"order" => array('Report.id DESC')));
						if ($action){
							$a[] = $action;
						}
					}
				}
				$a = md5(json_encode($a));
				if ($a != $has_md){
					$responce["data"] = $a;
				}
			}catch(Exception $error){
				$responce["error"] = 1;
				$responce["data"] = $error->getMessage();
			}
			$this->rend($responce);
		}

		public function getReportsByIdRoom ($token = ""){
			$responce = array("error" => 0,"data" => "");
			try{
				if ($token == ""){
					throw new Exception ("token not found");
				}
				if (!isset($_POST["idRoom"])){
					throw new Exception ("idRoom not found");
				}

				$idRoom = $_POST["idRoom"];
				$this->loadModel("People");
				$user = $this->People->find("first",array("conditions"=>array("People.has_md" => $token)));
				if ($user == null){
					throw new Exception ("User not found");
				}
				$this->loadModel("Report");
				$this->loadModel("Room");
				$room = $this->Room->find("first",array("conditions" => array("Room.id"=>$idRoom,"Room.person_id" => $user["People"]["id"])));
				$reports = $this->Report->find("all",array("conditions" => array("Report.room_id"=>$room["Room"]["id"])));
				$responce["data"] = $reports;
			}catch(Exception $error){
				$responce["error"] = 1;
				$responce["data"] = $error->getMessage();
			}
			$this->rend($responce);
		}

		public function setActionByRoom ($token = ""){
			$responce = array("error" => 0,"data" => "");
			try{
				if ($token == ""){
					throw new Exception ("token not found");
				}

				if (!isset($_POST["idRoom"])){
					throw new Exception ("idRoom not found");
				}

				if (!isset($_POST["idAction"])){
					throw new Exception ("idAction not found");
				}

				$idRoom = $_POST["idRoom"];
				$idAction = $_POST["idAction"];
				$this->loadModel("People");
				$user = $this->People->find("first",array("conditions"=>array("People.has_md" => $token)));
				$this->loadModel("Room");
				$this->loadModel("Report");
				$room = $this->Room->find("first",array("conditions" => array("Room.id"=>$idRoom,"Room.person_id" => $user["People"]["id"])));
				$this->Report->save(array("room_id" => $room["Room"]["id"], "state_id" => $idAction));
			}catch(Exception $error){
				$responce["error"] = 1;
				$responce["data"] = $error->getMessage();
			}
			$this->rend($responce);
		}
	}