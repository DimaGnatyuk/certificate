<?php
	class MainController extends AppController {
		private $USER = "user";
		private function hasMd ($login, $password){
			return md5("$login+lol+$password");
		}

		public function index (){
			$page = "auntification";
			if($this->Session->check($this->USER)){
				$page = "listRooms";
			}
			return $this->redirect($page);
		}

		public function auntification (){
			
		}

		public function login (){
			if (!isset($_POST["email"])){
				throw new Exception("email - null");
			}
			if (!isset($_POST["password"])){
				throw new Exception("password - null");
			}

			$email = $_POST["email"];
			//$email = $_POST["login"] = "testlogin@com.ua";
			$password = $_POST["password"];
			//$password = $_POST["password"] = "123456789";
			$this->loadModel("People");
			$people = $this->People->find("first",array("conditions" => array("People.has_md"=>$this->hasMd($email,$password))));
			if ($people == null)
			{
				throw new Exception ("User not found");
			}
			$this->Session->write($this->USER,$people);
			return $this->redirect("index");
		}

		public function registration (){

				if (!isset($_POST["name"])){
					throw new Exception("name - null");
				}
				if (!isset($_POST["email"])){
					throw new Exception("email - null");
				}
				if (!isset($_POST["password1"])){
					throw new Exception("password1 - null");
				}
				if (!isset($_POST["password2"])){
					throw new Exception("password2 - null");
				}
				if ($_POST["password1"] != $_POST["password2"]){
					throw new Exception("password - not correct");
				}
				$name = $_POST["name"];
				$login = $_POST["email"];
				//$login = $_POST["login"] = "testlogin@com.ua";
				$password = $_POST["password1"];
				//$password = $_POST["password"] = "123456789";
				$this->loadModel("People");
				//debug(md5("$login+lol+$password"));
				$people = $this->People->saveAll(array("name" => $name, "email"=>$login, "has_md" => $this->hasMd($login,$password)));
				if ($people == null)
				{
					throw new Exception ("User not found");
				}
				$this->Session->write($this->USER,$people["People"]);
				return $this->redirect("index");
		}

		public function logout (){
			if($this->Session->check($this->USER)){
				$this->Session->delete($this->USER);
			}
			return $this->redirect("index");
		}

		public function listRooms (){
			$user = $this->Session->read($this->USER);
			if ($user == null){
				return $this->redirect("index");
			}
			$rooms = array();
			$this->loadModel("People");
			$people = $this->People->find("first",array("conditions"=>array("People.id" => $user["People"]["id"])));
			if ($people){
				for ($i=0;$i<count($people["Room"]);$i++){
					$this->loadModel("Report");
					$action = $this->Report->find("first", array("conditions" => array("Report.room_id" => $people["Room"][$i]["id"]),"order" => array('Report.id DESC')));
					if ($action != null){
						$people["Room"][$i]["LastAction"] = $action;
					}
				}
				$rooms = $people["Room"];
			}
			$this->set(compact("rooms"));
		}

		public function viewRoom ($idRoom = null){
			$user = $this->Session->read($this->USER);
			if ($user == null){
				return $this->redirect("index");
			}
			$this->loadModel("Room");
			$this->Room->recursive = 2;
			$room = $this->Room->find("first",array("conditions"=>array("Room.id" => $idRoom, "Room.person_id" => $user["People"]["id"])));
			$this->set(compact("room"));
		}

		public function viewAddRoom (){

		}

		public function addRoom (){
			$page = "listRooms";
			$user = $this->Session->read($this->USER);
			if (!$user){
				$page = "index";
			}else{
				$name = $_POST["name"];
				$this->loadModel("Room");
				$this->Room->save(array("person_id" => $user["People"]["id"], "name" => $name));
			}
			return $this->redirect($page);
		}

		public function viewEditRoom ($idRoom){
			$user = $this->Session->read($this->USER);
			if ($user == null){
				return $this->redirect("index");
			}
			$this->loadModel("Room");
			$room = $this->Room->find("first",array("conditions"=>array("Room.id" => $idRoom, "Room.person_id" => $user["People"]["id"])));
			$this->set(compact("room"));
		}

		public function editRoom ($idRoom){
			$page = "/main/viewRoom/$idRoom";
			$user = $this->Session->read($this->USER);
			if ($user == null){
				$page = "index";
			}
			$name = $_POST["name"];
			$this->loadModel("Room");
			$room = $this->Room->find("first",array("conditions"=>array("Room.id" => $idRoom, "Room.person_id" => $user["People"]["id"])));
			if ($room != null){
				$room["Room"]["name"] = $name;
				$this->Room->save($room["Room"]);
			}
			return $this->redirect($page);
		}

		public function deleteRoom ($idRoom){
			$page = "listRooms";
			$user = $this->Session->read($this->USER);
			if ($user == null){
				$page = "index";
			}
			$this->loadModel("Room");
			$room = $this->Room->find("first",array("conditions"=>array("Room.id" => $idRoom, "Room.person_id" => $user["People"]["id"])));
			if ($room != null){
				$this->Room->delete($room["Room"]);
			}
			return $this->redirect($page);
		}
	}