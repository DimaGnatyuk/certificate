package com.example.admin.secret_castleclient.Elements.InternetLoaders;

import android.os.AsyncTask;

import com.example.admin.secret_castleclient.Elements.Configs.HTTP_ADR;
import com.example.admin.secret_castleclient.Elements.InternetLoaders.Interfaces.LoginInterface;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Created by Admin on 03.05.2016.
 */
public class LoginLoader extends AsyncTask<String, Void, Response> {
    private String login;
    private String password;
    private LoginInterface loginInterface;

    public LoginLoader(String login, String password, LoginInterface loginInterface) {
        this.login = login;
        this.password = password;
        this.loginInterface = loginInterface;
    }

    @Override
    protected Response doInBackground(String[] params) {
        Response response = null;
        try {

            OkHttpClient client = new OkHttpClient.Builder()
                    .build();

            response = client.newCall(HTTP_ADR.LogIn(login,password)).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(Response response) {
        super.onPostExecute(response);
        loginInterface.callBack(response);
    }
}
