package com.example.admin.secret_castleclient.Elements.CustomAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.secret_castleclient.Elements.DataBase.Room;
import com.example.admin.secret_castleclient.Elements.Managers.ManagerIMG;
import com.example.admin.secret_castleclient.R;

import java.util.List;

/**
 * Created by Admin on 03.05.2016.
 */
public class RoomsAddapter extends ArrayAdapter<Room> {

    private List<Room> roomList;
    private Context context;

    public RoomsAddapter(Context context, int resource, List<Room> objects) {
        super(context, resource, objects);
        this.roomList = objects;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_status_object, parent, false);
        Room room = roomList.get(position);

        TextView textViewName = (TextView) rowView.findViewById(R.id.name_list);
        TextView textViewStatusName = (TextView) rowView.findViewById(R.id.action_list);
        TextView textViewStatusData = (TextView) rowView.findViewById(R.id.data_list);
        ImageView imageViewStatusIcon = (ImageView) rowView.findViewById(R.id.image_list);

        textViewName.setText(room.getName());

        textViewStatusName.setText(room.getLastAction().getAction());
        textViewStatusData.setText(room.getLastAction().getData());
        imageViewStatusIcon.setImageResource(ManagerIMG.analizeId(room.getLastAction().getId()));
        //textViewName.setText(room.getName());


        //textView.setText(values[position]);
        // Изменение иконки для Windows и iPhone
        //String s = values[position];
        /*if (s.startsWith("Windows7") || s.startsWith("iPhone")
                || s.startsWith("Solaris")) {
            imageView.setImageResource(R.drawable.no);
        } else {
            imageView.setImageResource(R.drawable.ok);
        }*/

        return rowView;
    }
}
