package com.example.admin.secret_castleclient.Elements.DataBase;

import java.util.Date;

/**
 * Created by Admin on 03.05.2016.
 */
public class Status {
    private int id;
    private String action;
    private String data;
    private boolean is_warning;

    public Status() {
    }

    public Status(String action) {
        this.id = -5;
        this.action = action;
        this.data = new Date().toString();
        this.is_warning = true;
    }

    public Status(int id, String action, String data, boolean is_warning) {
        this.id = id;
        this.action = action;
        this.data = data;
        this.is_warning = is_warning;
    }

    public int getId() {
        return id;
    }

    public String getAction() {
        return action;
    }

    public String getData() {
        return data;
    }

    public boolean is_warning() {
        return is_warning;
    }

}
