package com.example.admin.secret_castleclient.Elements.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.example.admin.secret_castleclient.Elements.Configs.Config;
import com.example.admin.secret_castleclient.Elements.Configs.HTTP_ADR;
import com.example.admin.secret_castleclient.Elements.DataBase.User;
import com.example.admin.secret_castleclient.Elements.Managers.UserManager;
import com.example.admin.secret_castleclient.MainActivity;
import com.example.admin.secret_castleclient.R;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Response;

public class AutoSyncService extends Service {
    private static final String KEY_HAS_MD = "KEY_HAS_MD";
    private SharedPreferences sharedPreferences;
    private Gson gson = new Gson();

    private NotificationManager mNM;

    // Unique Identification Number for the Notification.
    // We use it on Notification start, and to cancel it.
    private int NOTIFICATION = 12345;

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class LocalBinder extends Binder {
        AutoSyncService getService() {
            return AutoSyncService.this;
        }
    }

    @Override
    public void onCreate()
    {
        // Запуск потока, на котором работает служба. Обратите внимание,
        // что мы создаем отдельный поток, потому что по умолчанию служба
        // работает в главном потоке процесса, который мы не хотим
        // блокировать. Мы также даем ему приоритет background, так чтобы
        // интенсивное задействование CPU не ухудшило работу UI.
        mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        // Display a notification about us starting.  We put an icon in the status bar.
        //showNotification();
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                1);
        thread.start();

        // Получение Looper для HandlerThread, и использование его для
        // нашего Handler.
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        //Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();

        // Для каждого запущенного запроса отправляется сообщение для запуска
        // выполнения работы, и передачи start ID, чтобы мы знали, какой
        // запрос связан с остановкой, когда вся работа завершена.
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);

        // Если мы прибиты, то после возврата отсюда произойдет рестарт.
        return START_STICKY;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new LocalBinder();

    /**
     * Show a notification while this service is running.
     */
    private void showNotification() {
        // In this sample, we'll use the same text for the ticker and the expanded notification
        CharSequence text = "Увага";

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);

        // Set the info for the views that show in the notification panel.
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(R.drawable.warning)  // the status icon
                .setTicker(text)  // the status text
                .setWhen(System.currentTimeMillis())  // the time stamp
                .setDefaults(Notification.DEFAULT_SOUND).setAutoCancel(true)
                .setContentTitle("Зафіксоване втручання")  // the label of the entry
                .setContentText(text)  // the contents of the entry
                .setContentIntent(contentIntent)  // The intent to send when the entry is clicked
                .build();

        // Send the notification.
        mNM.notify(NOTIFICATION, notification);
    }

    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;

    // Обработчик, который принимает сообщения от потока.
    private final class ServiceHandler extends Handler
    {
        public ServiceHandler(Looper looper)
        {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg)
        {
            sharedPreferences = getSharedPreferences(Config.APP_CONFIG,MODE_PRIVATE);
            String userObjJson = sharedPreferences.getString(Config.USER_OBJECT, "");
            if (userObjJson.compareToIgnoreCase("") == 0){
                stopSelf();
            }

            UserManager.setUser(gson.fromJson(userObjJson, User.class));
            // По правилам мы должны здесь сделать некую работу,
            // типа загрузки файла.
            // В нашем примере мы просто засыпаем на 5 секунд.
            while (true) {
                OkHttpClient client = new OkHttpClient();

                Response response = null;
                try {


                    String has = sharedPreferences.getString(KEY_HAS_MD, "");
                    response = client.newCall(HTTP_ADR.AllTest(UserManager.getInstance().getToken(),has)).execute();
                    String s = response.body().string();
                    JSONObject jsonObject = new JSONObject(s);
                    if (jsonObject.getInt("error") == 0){
                        if (jsonObject.getString("data").compareToIgnoreCase("") != 0){
                            showNotification();
                            sharedPreferences.edit().putString(KEY_HAS_MD, jsonObject.getString("data")).commit();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                long endTime = System.currentTimeMillis() + 5 * 1000;
                while (System.currentTimeMillis() < endTime) {
                    synchronized (this) {
                        try {
                            wait(endTime - System.currentTimeMillis());
                        } catch (Exception e) {
                        }
                    }
                }
            }
        }
    }

}
