package com.example.admin.secret_castleclient.Elements.Configs;

import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by Admin on 03.05.2016.
 */
public class HTTP_ADR {
    public static Request LogIn (String login,String password){
        RequestBody formBody = new FormBody.Builder()
                .add("email", login)
                .add("password", password)
                .build();

        Request request = new Request.Builder()
                .url(Config.WEB_ADR+"auntification")
                .post(formBody)
                .build();
        return request;
    }

    public static Request GetAllRooms (String key){
        Request request = new Request.Builder()
                .url(Config.WEB_ADR + "getAllRooms/" + key)
                .build();
        return request;
    }

    public static Request AllTest (String key,String has){
        Request request = new Request.Builder()
                .url(Config.WEB_ADR+"getTestStatus/"+key+"/"+has)
                .build();
        return request;
    }

    public static Request GetRoomInfo (String key,int id){
        RequestBody formBody = new FormBody.Builder()
                .add("idRoom", String.valueOf(id))
                .build();

        Request request = new Request.Builder()
                .url(Config.WEB_ADR+"getReportsByIdRoom/"+key)
                .post(formBody)
                .build();
        return request;
    }
}
