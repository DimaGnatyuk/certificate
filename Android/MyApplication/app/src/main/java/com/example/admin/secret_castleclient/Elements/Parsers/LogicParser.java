package com.example.admin.secret_castleclient.Elements.Parsers;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 08.06.2016.
 */
public class LogicParser {
    public static boolean isValid (JSONObject jsonObject) throws JSONException {
        return jsonObject.getInt("error") == 0;
    }
}
