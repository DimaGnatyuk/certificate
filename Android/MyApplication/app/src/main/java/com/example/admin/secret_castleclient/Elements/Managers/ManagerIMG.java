package com.example.admin.secret_castleclient.Elements.Managers;

import com.example.admin.secret_castleclient.R;

/**
 * Created by Admin on 09.06.2016.
 */
public class ManagerIMG {
    public static int analizeId (int id){
        int val = 0;
        switch (id){
            case -5: {
                val = R.drawable.open;
                break;
            }
            case -3: {
                val = R.drawable.error;
                break;
            }
            case -1: {
                val = R.drawable.warning;
                break;
            }
            case 0: {
                val = R.drawable.closed;
                break;
            }
            case 1: {
                val = R.drawable.open;
                break;
            }
            case 5: {
                val = R.drawable.sync;
                break;
            }
            default:{
                val = R.drawable.open;
            }
        }
        return val;
    }
}
