package com.example.admin.secret_castleclient;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.admin.secret_castleclient.Elements.Configs.Config;
import com.example.admin.secret_castleclient.Elements.CustomAdapter.StatusAdapter;
import com.example.admin.secret_castleclient.Elements.DataBase.Room;
import com.example.admin.secret_castleclient.Elements.DataBase.Status;
import com.example.admin.secret_castleclient.Elements.InternetLoaders.Interfaces.StatusInterface;
import com.example.admin.secret_castleclient.Elements.InternetLoaders.StatusLoader;
import com.example.admin.secret_castleclient.Elements.Managers.UserManager;
import com.example.admin.secret_castleclient.Elements.Parsers.MainParser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Response;

public class StatusActivity extends AppCompatActivity implements StatusInterface {

    private static final String JSON_SAVE_LIST_ACTIONS = "statusList";
    private static final String JSON_SAVE_ROOM_ID = "room_ID";

    private ListView listView;
    private List<Status> statusList = new ArrayList();
    private List<Room> roomList = new ArrayList();
    private ListView mDrawerListView;
    private int idRoom = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);
        listView = (ListView)findViewById(R.id.listStatus);
        mDrawerListView = (ListView)findViewById(R.id.left_drawer);
        setTitle(getResources().getText(R.string.Logs));
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                int item = roomList.get(position).getId();
                bundle.putInt(Config.TRANSPORT_ROOM_ID, item);
                Gson gson = new Gson();
                bundle.putString(Config.ROOMS, gson.toJson(roomList));
                Intent intent = new Intent(view.getContext(), StatusActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        Bundle bundle = this.getIntent().getExtras();
        String token = "";
        try {
            if( savedInstanceState != null )
            {
                if( savedInstanceState.containsKey( JSON_SAVE_LIST_ACTIONS ) )
                {
                    Gson gson = new Gson();
                    Type type1 = new TypeToken<List<Status>>(){}.getType();
                    Type type2 = new TypeToken<List<Room>>(){}.getType();
                    statusList = gson.fromJson(savedInstanceState.getString(JSON_SAVE_LIST_ACTIONS),type1);
                    roomList = gson.fromJson(savedInstanceState.getString(Config.ROOMS),type2);
                    idRoom  = savedInstanceState.getInt(JSON_SAVE_ROOM_ID,-1);
                    loadList ();
                }
            }else{
                idRoom = bundle.getInt(Config.TRANSPORT_ROOM_ID, -1);
                Gson gson = new Gson();
                Type type = new TypeToken<List<Room>>(){}.getType();
                roomList = gson.fromJson(bundle.getString(Config.ROOMS),type);
                token = UserManager.getInstance().getToken();
                if ((token.compareToIgnoreCase("") == 0) || (idRoom == -1)){
                    throw new Exception("Fatal Error");
                }
                new StatusLoader(token,(StatusInterface) this,idRoom).execute();
            }

        } catch (Exception e) {
            finish();
            return;
        }
    }

    private void loadList (){
        if (statusList != null) {
            StatusAdapter statusAdapter = new StatusAdapter(this, statusList.size(), statusList);
            listView.setAdapter(statusAdapter);
            List<String> stringList = new ArrayList<>();
            for(Room room :roomList){
                stringList.add(room.getName());
                if (room.getId() == idRoom){
                    setTitle(room.getName());
                }
            }
            mDrawerListView.setAdapter(new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, stringList));
        }
    }

    @Override
    public void callBack(Response response) throws IOException {
        try {
            JSONObject rootElement = new JSONObject(response.body().string());
            statusList = MainParser.parseStates(rootElement);
            loadList();
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
    }

    protected void onSaveInstanceState( Bundle outState )
    {
        Gson gson = new Gson();
        String listS = gson.toJson(statusList);
        String listR = gson.toJson(roomList);
        outState.putString(JSON_SAVE_LIST_ACTIONS, listS);
        outState.putString(Config.ROOMS, listR);
        outState.putInt(JSON_SAVE_ROOM_ID, idRoom);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:{
                finish();
                return true;
            }
            case R.id.refresh: {
                try {
                    new StatusLoader( UserManager.getInstance().getToken(),(StatusInterface) this,idRoom).execute();
                } catch (Exception e) {
                    startActivity(new Intent(this, LoginActivity.class));
                    finish();
                }
                return true;
            }
            case R.id.exit:{
                getSharedPreferences(Config.APP_CONFIG,MODE_PRIVATE).edit().clear().commit();
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
