package com.example.admin.secret_castleclient;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.admin.secret_castleclient.Elements.Configs.Config;
import com.example.admin.secret_castleclient.Elements.CustomAdapter.RoomsAddapter;
import com.example.admin.secret_castleclient.Elements.DataBase.Room;
import com.example.admin.secret_castleclient.Elements.DataBase.User;
import com.example.admin.secret_castleclient.Elements.InternetLoaders.Interfaces.MainInterface;
import com.example.admin.secret_castleclient.Elements.InternetLoaders.MainLoader;
import com.example.admin.secret_castleclient.Elements.Managers.UserManager;
import com.example.admin.secret_castleclient.Elements.Parsers.MainParser;
import com.example.admin.secret_castleclient.Elements.Services.AutoSyncService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements MainInterface {

    private static final String JSON_SAVE_LIST_ROOM = "list_rooms";

    private ListView listView;
    private List<Room> roomList = null;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(getResources().getText(R.string.Rooms));
        listView = (ListView)findViewById(R.id.listRooms);

        sharedPreferences = getSharedPreferences(Config.APP_CONFIG,MODE_PRIVATE);
        String userObjJson = sharedPreferences.getString(Config.USER_OBJECT, "");

        if (userObjJson.compareToIgnoreCase("") == 0){
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }else{
            if( savedInstanceState != null )
            {
                if( savedInstanceState.containsKey( JSON_SAVE_LIST_ROOM ) )
                {
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<Room>>(){}.getType();
                    roomList = gson.fromJson(savedInstanceState.getString(JSON_SAVE_LIST_ROOM),type);
                    loadList ();
                }
            }else {
                if (roomList == null){
                    Gson gson = new Gson();
                    UserManager.setUser(gson.fromJson(userObjJson, User.class));
                    try {
                        new MainLoader((MainInterface) this,UserManager.getInstance().getToken()).execute();
                    } catch (Exception e) {
                        startActivity(new Intent(this, LoginActivity.class));
                        finish();
                    }
                }
            }
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                int item = roomList.get(position).getId();
                bundle.putInt(Config.TRANSPORT_ROOM_ID, item);
                Gson gson = new Gson();
                bundle.putString(Config.ROOMS, gson.toJson(roomList));
                Intent intent = new Intent(view.getContext(), StatusActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

    }

    protected void onSaveInstanceState( Bundle outState )
    {
        Gson gson = new Gson();
        String listS = gson.toJson(roomList);
        outState.putString(JSON_SAVE_LIST_ROOM, listS);
    }

    private void loadList (){
        if (roomList != null) {
            startService(new Intent(this, AutoSyncService.class));
            RoomsAddapter roomsAddapter = new RoomsAddapter(this, roomList.size(), roomList);
            listView.setAdapter(roomsAddapter);
        }
    }

    @Override
    public void callBack(Response response) throws IOException, JSONException {
        try {
            if (response == null){
                startActivity(new Intent(this,LoginActivity.class));
            }else{
                JSONObject jsonRoot = new JSONObject(response.body().string());
                this.roomList = MainParser.parseRooms(jsonRoot);
                loadList();
            }
        } catch (Exception e) {
            startActivity(new Intent(this,LoginActivity.class));
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.refresh:
                try {
                    new MainLoader((MainInterface) this,UserManager.getInstance().getToken()).execute();
                } catch (Exception e) {
                    startActivity(new Intent(this, LoginActivity.class));
                    finish();
                }
                return true;
            case R.id.exit:
                sharedPreferences.edit().clear().commit();
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
