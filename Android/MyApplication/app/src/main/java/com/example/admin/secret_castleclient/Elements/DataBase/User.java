package com.example.admin.secret_castleclient.Elements.DataBase;

/**
 * Created by Admin on 08.06.2016.
 */
public class User {
    private int id;
    private String name;
    private String token;
    private String email;

    public User() {
    }

    public User(int id, String name, String email, String token) {
        this.id = id;
        this.name = name;
        this.token = token;
        this.email = email;
    }

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getToken() {
        return token;
    }
    public String getEmail() {
        return email;
    }
}
