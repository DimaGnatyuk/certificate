package com.example.admin.secret_castleclient.Elements.Configs;

/**
 * Created by Admin on 03.05.2016.
 */
public class Config {
    public static final String APP_CONFIG = "SecretCastleClient";
    public static final String USER_OBJECT = "u_user_obj";
    public static final String USER_DATA = "u_user_data";
    public static final String TRANSPORT_ROOM_ID = "u_t_room_id";
    public static final String ROOMS = "t_rooms";

    public static final String WEB_ADR = "http://www.certificate-project.esy.es/api/";
}
