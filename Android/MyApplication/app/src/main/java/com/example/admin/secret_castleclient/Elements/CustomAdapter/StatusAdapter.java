package com.example.admin.secret_castleclient.Elements.CustomAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.secret_castleclient.Elements.DataBase.Status;
import com.example.admin.secret_castleclient.Elements.Managers.ManagerIMG;
import com.example.admin.secret_castleclient.R;

import java.util.List;

/**
 * Created by Admin on 03.05.2016.
 */
public class StatusAdapter extends ArrayAdapter<Status> {

    private Context context;
    private List<Status> statusList;

    public StatusAdapter(Context context, int resource, List<Status> objects) {
        super(context, resource, objects);
        this.context = context;
        this.statusList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_status_object, parent, false);
        Status status = statusList.get(position);

        TextView textViewName = (TextView) rowView.findViewById(R.id.name_list);
        TextView textViewStatusName = (TextView) rowView.findViewById(R.id.action_list);
        TextView textViewStatusData = (TextView) rowView.findViewById(R.id.data_list);
        ImageView imageViewStatusIcon = (ImageView) rowView.findViewById(R.id.image_list);
        textViewStatusData.setVisibility(View.INVISIBLE);
        textViewName.setText(status.getAction());
        textViewStatusName.setText(status.getData());
        imageViewStatusIcon.setImageResource(ManagerIMG.analizeId(status.getId()));
        return rowView;
    }
}
