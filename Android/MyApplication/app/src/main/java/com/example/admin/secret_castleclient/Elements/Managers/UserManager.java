package com.example.admin.secret_castleclient.Elements.Managers;

import com.example.admin.secret_castleclient.Elements.DataBase.User;

/**
 * Created by Admin on 08.06.2016.
 */
public class UserManager {
    private static User user = null;

    public static User getInstance () throws Exception {
        if (UserManager.user == null){
            throw new Exception("user - null");
        }
        return UserManager.user;
    }

    public static void setUser (User user){
        UserManager.user = user;
    }
}
