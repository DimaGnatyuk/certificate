package com.example.admin.secret_castleclient.Elements.Parsers;

import com.example.admin.secret_castleclient.Elements.DataBase.Room;
import com.example.admin.secret_castleclient.Elements.DataBase.Status;
import com.example.admin.secret_castleclient.Elements.DataBase.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 08.06.2016.
 */
public class MainParser {
    public static User parseUser (JSONObject jsonRoot) throws JSONException {
        User user = null;
        if (LogicParser.isValid(jsonRoot)){
            JSONObject jsonData = jsonRoot.getJSONObject("data");
            JSONObject people = jsonData.getJSONObject("People");
            int id = people.getInt("id");
            String name = people.getString("name");
            String email = people.getString("email");
            String token = people.getString("has_md");
            user = new User(id,name,email,token);
        }
        return user;
    }

    public static List<Room> parseRooms (JSONObject jsonRoot) throws JSONException {
        List<Room> rooms = new ArrayList<>();
        if (LogicParser.isValid(jsonRoot)){
            JSONArray jsonData = jsonRoot.getJSONArray("data");
            for (int i = 0; i<jsonData.length();i++) {
                JSONObject roomJson = jsonData.getJSONObject(i);
                Room room = new Room();
                room.setId(roomJson.getInt("id"));
                room.setName(roomJson.getString("name"));
                if (roomJson.getString("LastAction").compareToIgnoreCase("none") == 0){
                    room.setLastAction(new Status("Не зафіксовано активності."));
                }else{
                    room.setLastAction(parseState(roomJson.getJSONObject("LastAction")));
                }
                rooms.add(room);
            }

        }
        return rooms;
    }

    public static Status parseState (JSONObject jsonRoot) throws JSONException {
        Status status = null;
        JSONObject jsonState = jsonRoot.getJSONObject("State");
        JSONObject jsonReport = jsonRoot.getJSONObject("Report");
        status = new Status(jsonState.getInt("id"),jsonState.getString("name"),jsonReport.getString("data"),jsonState.getBoolean("isWarning"));
        return status;
    }

    public static List<Status> parseStates (JSONObject jsonRoot) throws JSONException {
        List<Status> statuses = new ArrayList<>();
        if (LogicParser.isValid(jsonRoot)) {
            JSONArray jsonArray = jsonRoot.getJSONArray("data");
            for(int i=0;i<jsonArray.length();i++)
            {
                Status status = parseState(jsonArray.getJSONObject(i));
                statuses.add(status);
            }
        }
        return statuses;
    }


}
