package com.example.admin.secret_castleclient.Elements.InternetLoaders.Interfaces;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.Response;

/**
 * Created by Admin on 03.05.2016.
 */
public interface MainInterface {
    void callBack (Response response) throws IOException, JSONException;
}
