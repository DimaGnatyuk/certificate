package com.example.admin.secret_castleclient.Elements.InternetLoaders;

import android.os.AsyncTask;

import com.example.admin.secret_castleclient.Elements.Configs.HTTP_ADR;
import com.example.admin.secret_castleclient.Elements.InternetLoaders.Interfaces.StatusInterface;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Created by Admin on 03.05.2016.
 */
public class StatusLoader extends AsyncTask<String, Void, Response> {

    private String key;
    private StatusInterface statusInterface;
    private int idRoom;

    public StatusLoader(String key, StatusInterface statusInterface, int idRoom) {
        this.key = key;
        this.statusInterface = statusInterface;
        this.idRoom = idRoom;
    }

    @Override
    protected Response doInBackground(String... params) {
        Response response = null;
        try {
            OkHttpClient client = new OkHttpClient.Builder()
                    .build();
            response = client.newCall(HTTP_ADR.GetRoomInfo(key, idRoom)).execute();
        } catch (Exception e) {
            e.printStackTrace();
            response = null;
        }
        return response;
    }

    @Override
    protected void onPostExecute(Response response) {
        super.onPostExecute(response);
        try {
            statusInterface.callBack(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
