package com.example.admin.secret_castleclient.Elements.DataBase;

import java.util.Collection;
import java.util.HashSet;

/**
 * Created by Admin on 03.05.2016.
 */
public class Room {
    private int id;
    private String name;
    private Status lastAction;
    private Collection<Status> statusCollection = new HashSet<Status>();

    public Room() {

    }

    public Room(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Room(int id, String name, Collection<Status> statusCollection) {
        this.id = id;
        this.name = name;
        this.statusCollection = statusCollection;
    }

    public Status getLastAction() {
        return lastAction;
    }

    public void setLastAction(Status lastAction) {
        this.lastAction = lastAction;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Status> getStatusCollection() {
        return statusCollection;
    }

    public void setStatusCollection(Collection<Status> statusCollection) {
        this.statusCollection = statusCollection;
    }
}
