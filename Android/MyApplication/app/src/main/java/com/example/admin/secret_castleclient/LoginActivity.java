package com.example.admin.secret_castleclient;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.admin.secret_castleclient.Elements.Configs.Config;
import com.example.admin.secret_castleclient.Elements.Elements.AllertDialogService;
import com.example.admin.secret_castleclient.Elements.InternetLoaders.Interfaces.LoginInterface;
import com.example.admin.secret_castleclient.Elements.InternetLoaders.LoginLoader;
import com.example.admin.secret_castleclient.Elements.Managers.UserManager;
import com.example.admin.secret_castleclient.Elements.Parsers.MainParser;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Date;

import okhttp3.Response;

public class LoginActivity extends AppCompatActivity implements LoginInterface {

    private EditText email_et;
    private EditText password_et;
    private Button logIn;
    private LoginInterface _this;

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle(getResources().getText(R.string.Auntification));
        _this = (LoginInterface)this;

        email_et = (EditText)findViewById(R.id.et_email_login);
        password_et = (EditText)findViewById(R.id.et_password_login);
        logIn = (Button)findViewById(R.id.logIn_btn_login);

        sharedPreferences = getSharedPreferences(Config.APP_CONFIG,MODE_PRIVATE);

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnline()) {
                    new LoginLoader(email_et.getText().toString(), password_et.getText().toString(), _this).execute();
                } else {
                    AllertDialogService.Create(v.getContext());
                }
            }
        });
    }

    @Override
    public void callBack(Response response) {
        try {
            JSONObject rootElement = new JSONObject(response.body().string());
            UserManager.setUser(MainParser.parseUser(rootElement));
            Gson gson = new Gson();
            String s = gson.toJson(UserManager.getInstance());
            Date date = new Date();
            sharedPreferences.edit()
                    .putString(Config.USER_OBJECT, s)
                    .putString(Config.USER_DATA, date.toString())
                    .commit();
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } catch (Exception e) {
            e.printStackTrace();
            String s = "Запит не було виконано";
            if (e.getMessage().compareToIgnoreCase("")!=0){
                s = e.getMessage();
            }
            AllertDialogService.Create(this,"Помилка",s);
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }
}
