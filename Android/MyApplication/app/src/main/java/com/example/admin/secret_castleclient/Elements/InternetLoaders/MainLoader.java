package com.example.admin.secret_castleclient.Elements.InternetLoaders;

import android.os.AsyncTask;

import com.example.admin.secret_castleclient.Elements.Configs.HTTP_ADR;
import com.example.admin.secret_castleclient.Elements.InternetLoaders.Interfaces.MainInterface;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Created by Admin on 03.05.2016.
 */
public class MainLoader extends AsyncTask<String, Void, Response> {

    private MainInterface mainInterface;
    private String key;

    public MainLoader(MainInterface mainInterface,String key) {
        this.mainInterface = mainInterface;
        this.key = key;
    }

    @Override
    protected Response doInBackground(String... params) {
        Response response = null;
        try {

            OkHttpClient client = new OkHttpClient.Builder()
                    .build();

            response = client.newCall(HTTP_ADR.GetAllRooms(key)).execute();

        } catch (Exception e) {
            e.printStackTrace();
            response = null;
        }
        return response;
    }

    @Override
    protected void onPostExecute(Response response) {
        super.onPostExecute(response);
        try {
            this.mainInterface.callBack(response);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
