package com.example.admin.secret_castleclient.Elements.Elements;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.example.admin.secret_castleclient.R;

/**
 * Created by Admin on 10.06.2016.
 */
public class AllertDialogService {

    public static void Create (Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Помилка!")
                .setMessage("Неможливо отримати доступ до інтернета!")
                .setIcon(R.drawable.warning)
                .setCancelable(false)
                .setNegativeButton("Закрити",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void Create (Context context,String title,String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
                .setMessage(message)
                .setIcon(R.drawable.warning)
                .setCancelable(false)
                .setNegativeButton("Закрити",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
