package com.example.admin.secret_castleclient.Elements.InternetLoaders.Interfaces;

import java.io.IOException;

import okhttp3.Response;

/**
 * Created by Admin on 03.05.2016.
 */
public interface StatusInterface {
    void callBack (Response response) throws IOException;
}
