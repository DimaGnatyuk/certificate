﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Ports;
using Secret_Castle.Elements;
using Secret_Castle.Elements.Model;
using Secret_Castle.Elements.Controller;

namespace Secret_Castle
{

    public partial class MainWindow : Window
    {
        int baud_rate = 9600;
        private SerialPort serialPort = null;
        private User user = null;
        private SelectRoom _selectRoom = new SelectRoom();
        private MediaPlayer player = new MediaPlayer();

        public MainWindow()
        {
            if (ManagerAccount.getIntent() == null || ManagerAccount.getIntent().getUser() == null)
            {
                login login = new login();
                login.ShowDialog();
                if (ManagerAccount.getIntent() == null)
                {
                    this.Close();
                    return;
                }
                this.user = ManagerAccount.getIntent().getUser();
            }
            this.user = ManagerAccount.getIntent().getUser();

            if (user != null)
            {
                if (_selectRoom.getRoom() == null)
                {
                    Secret_Castle.selectRoom selectRoomGUI = new selectRoom(this.user);
                    selectRoomGUI.ShowDialog();
                    _selectRoom.setRoom(selectRoomGUI.getRoom());
                }

                if (_selectRoom.getRoom() != null)
                {
                    InitializeComponent();
                    loadProgremElements();
                }
                else
                {
                    this.Close();
                }
            }else
            {
                this.Close();
            }
        }

        private void loadProgremElements()
        {
            loadPortsList();
            loadHistoryRoom();
        }

        private void loadPortsList() {
            String[] ports = SerialPort.GetPortNames();

            foreach(String port in ports) {
                comboBox.Items.Add(port);
            }
        }

        private void loadHistoryRoom()
        {
            textBox.Text = "Start log..";
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (button1.Content.ToString() == "Connect")
            {
                try
                {
                    textBox.Text = "Connect" + "\n" + textBox.Text;
                    button1.Content = "Disconnect";
                    serialPort = new SerialPort();
                    serialPort.PortName = comboBox.Text;
                    serialPort.BaudRate = Convert.ToInt32(baud_rate);
                    serialPort.DataReceived += SerialPort_DataReceived;
                    serialPort.Open();
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }else
            {
                textBox.Text = "Disconnect" + "\n" + textBox.Text;
                button1.Content = "Connect";
                serialPort.Close();
            }
        }

        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (serialPort.IsOpen) {
                int status = Convert.ToInt32(serialPort.ReadLine());
                updateDebug(status.ToString() + " - " + DateTime.Now.ToString());
                string resault = WebComponents.Post(URLAddres.UrlSetActionByRoom(this.user.token), URLAddres.ContainerSetActionByRoom(_selectRoom, status));
                updateDebug("Send to Server" + " - " + resault.Replace("/n",""));
                soung(0);
            }
            else
            {
                MessageBox.Show("Port - Closed...");
            }
        }

        private void updateDebug(string obj)
        {
            textBox.Dispatcher.BeginInvoke(new System.Action(delegate ()
            {
                textBox.Text = obj + "\n" + textBox.Text;
            }));
        }

        private void soung (int statys)
        {
            if (statys == -1) {
                player.Dispatcher.BeginInvoke(new System.Action(delegate ()
                {
                    player.Open(new Uri(@"res\warm.mp3", UriKind.Relative));
                    player.Volume = 1;
                    player.Play();
                }));
            }else if(statys == -3)
            {
                player.Dispatcher.BeginInvoke(new System.Action(delegate ()
                {
                    player.Open(new Uri(@"res\error.mp3", UriKind.Relative));
                    player.Volume = 1;
                    player.Play();
                }));
            }else if (statys == 5)
            {
                player.Dispatcher.BeginInvoke(new System.Action(delegate ()
                {
                    player.Open(new Uri(@"res\ok.mp3", UriKind.Relative));
                    player.Volume = 1;
                    player.Play();
                }));
            }
            
        }


    }
}
