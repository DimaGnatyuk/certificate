﻿using Secret_Castle.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using Secret_Castle.Elements.Model;
using Secret_Castle.Elements.Controller;
using System.Threading.Tasks;

namespace Secret_Castle
{
    /// <summary>
    /// Interaction logic for login.xaml
    /// </summary>
    public partial class login : Window
    {
        public login()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.button.Content = "Loading...";
                string resault =  WebComponents.Post(URLAddres.UrlLogIn(), URLAddres.ContainerLogIn(textBox2.Text, textBox1.Password));
                ManagerAccount.setUser(ConvertorsModel.GetUser(resault));
                this.Close();
            }catch(Exception error)
            {
                button.Content = "Login";
                MessageBox.Show(error.Message);
            }
        }

    }
}
