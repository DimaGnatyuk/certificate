﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Secret_Castle.Elements;
using Secret_Castle.Elements.Model;
using Secret_Castle.Elements.Controller;

namespace Secret_Castle
{
    /// <summary>
    /// Логика взаимодействия для selectRoom.xaml
    /// </summary>
    public partial class selectRoom : Window
    {
        private User user = null;
        private Room room = null;
        public selectRoom(User user)
        {
            InitializeComponent();
            
            string resault = WebComponents.Get(URLAddres.UrlLoadAllRooms(user.token.ToString()));
            user = ConvertorsModel.GetUserRoom(resault, user);
            ManagerAccount.setUser(user);
            this.user = user;
            loadConfig();
        }

        public selectRoom()
        {
            InitializeComponent();
        }

        private void loadConfig()
        {
            loadRooms();
        }

        private void loadRooms()
        {
            foreach (var room in this.user.rooms)
            {
                comboBox.Items.Add(room.name.ToString());
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {   
            foreach (var roomObj in this.user.rooms)
            {
                if (comboBox.Text == roomObj.name.ToString())
                {
                    this.room = new Room();
                    this.room.id = roomObj.id;
                    this.room.name = roomObj.name;
                    this.Close();
                    return;
                }
            }
            
        }

        internal Room getRoom()
        {
            return this.room;
        }
    }
}
