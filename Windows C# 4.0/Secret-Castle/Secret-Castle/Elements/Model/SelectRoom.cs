﻿namespace Secret_Castle.Elements.Model
{
    public class SelectRoom
    {
        private Room room;

        public void setRoom(Room room)
        {
            this.room = room;
        }
        public Room getRoom()
        {
            return this.room;
        }
    }
}