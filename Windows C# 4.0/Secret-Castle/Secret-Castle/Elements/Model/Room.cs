﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Secret_Castle.Elements.Model
{
    public class Room
    {
        public int id { set; get; }
        public string name { set; get; }
        public ICollection<History> historys = new HashSet<History>();
    }
}
