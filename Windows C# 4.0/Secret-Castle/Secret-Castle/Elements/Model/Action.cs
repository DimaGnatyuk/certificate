﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Secret_Castle.Elements.Model
{
    public class Action
    {
        public int id { set; get; }
        public string name { set; get; }
        public string action { set; get; }
        public bool isWarning { set; get; }
    }
}
