﻿using Secret_Castle.Elements.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Secret_Castle.Elements
{
    public class User
    {
        public int id { set; get; }
        public string name { set; get; }
        public string token { set; get; }
        public ICollection<Room> rooms = new HashSet<Room>();
    }
}
