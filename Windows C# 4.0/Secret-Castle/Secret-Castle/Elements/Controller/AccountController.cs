﻿using Secret_Castle.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Secret_Castle.Elements
{
    class AccountController
    {
        private User user = null;

        public AccountController(User user)
        {
            this.user = user;
        }

        public User getUser()
        {
            return user;
        }
    }
}
