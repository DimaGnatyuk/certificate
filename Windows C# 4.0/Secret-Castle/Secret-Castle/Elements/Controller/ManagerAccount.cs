﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Secret_Castle.Elements.Controller
{
    class ManagerAccount
    {
        private static AccountController accountController = null;

        public static AccountController getIntent()
        {
            return accountController;
        }

        public static void setUser(User user)
        {
            accountController = new AccountController(user);
        }
    }
}
