﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;

namespace Secret_Castle.Elements
{
    public class WebComponents
    {
        public static string Get(string url)
        {
            string response;
            try
            {
                using (var webClient = new WebClient())
                {
                    response = webClient.DownloadString(url);
                }
            }
            catch (Exception error)
            {
                response = error.Message;
            }
            return response;
        }

        public static string Post(string url, NameValueCollection postParameters)
        {
            string resault = "";
             try
            {
                using (var webClient = new WebClient())
                {
                    var response = webClient.UploadValues(url, postParameters);
                    resault = System.Text.Encoding.UTF8.GetString(response);
                }
            }
             catch (Exception error)
             {
                 resault = error.Message;
             }
             return resault;
        }
    }
}