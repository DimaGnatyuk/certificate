﻿using Secret_Castle.Elements.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace Secret_Castle.Elements
{
    public static class URLAddres
    {
        private static string headUsr = "http://certificate-project.esy.es/";
        private static string apiUsr = "api/";
        private static string baseUsr = headUsr+ apiUsr;

        public static string UrlLogIn() {
            return baseUsr + "auntification";
        }

        public static string UrlSetActionByRoom(string token)
        {
            return baseUsr + "setActionByRoom/"+ token;
        }

        public static string UrlLoadAllRooms(string token)
        {
            return baseUsr + "getAllRooms" + "/" + token;
        }

        public static string UrlGetReportsByIdRoom(int id, string token)
        {
            return baseUsr + "getReportsByIdRoom/" + Convert.ToString(id) + "/" + token;
        }

        public static string UrlLoadListActionByRoom(int idRoom, string token)
        {
            return baseUsr + "getReportsByIdRoom/" + Convert.ToString(idRoom) + "/" + token;
        }

        public static NameValueCollection ContainerLogIn(string login, string password)
        {
            NameValueCollection value = new NameValueCollection();
            value.Add("format", "json");
            value.Add("email", login);
            value.Add("password", password);
            return value;
        }

        public static NameValueCollection ContainerSetActionByRoom(SelectRoom selectRoom,int idAction)
        {
            idAction = (idAction == 0 || idAction == 1 || idAction == -1 || idAction == -3 || idAction == 5) ? idAction : -3;
            Room root = selectRoom.getRoom();
            NameValueCollection value = new NameValueCollection();
            value.Add("format", "json");
            value.Add("idRoom", root.id.ToString());
            value.Add("idAction", idAction.ToString());
            return value;
        }
    }
}
