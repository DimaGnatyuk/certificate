﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Secret_Castle.Elements.Model;

namespace Secret_Castle.Elements
{
    class ConvertorsModel
    {
        public static User GetUser(string json)
        {
            User user = null;
            dynamic value = JsonConvert.DeserializeObject(json);
            var errorStatus = value.error;
            if (errorStatus == 1)
            {
                string e = value.data;
                throw new Exception(e);
            }
            else
            {
                user = new User();
                user.id = value.data.People.id;
                user.name = value.data.People.name;
                user.token = value.data.People.has_md;
            }
            return user;
        }

        public static User GetUserRoom(string json,User user)
        {
            dynamic value = JsonConvert.DeserializeObject(json);
            var errorStatus = value.error;
            if (errorStatus == 1)
            {
                string e = value.data;
                throw new Exception(e);
            }
            else
            {
                foreach (dynamic temp in value.data)
                {
                    Room room = new Room();
                    room.id = temp.id;
                    room.name = temp.name;
                    user.rooms.Add(room);
                }
            }
            return user;
        }
    }
}
