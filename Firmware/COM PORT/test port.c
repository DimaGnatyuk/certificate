/*******************************************************
This program was created by the
CodeWizardAVR V3.10 Advanced
Automatic Program Generator
� Copyright 1998-2014 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project : 
Version : 
Date    : 06.06.2016
Author  : 
Company : 
Comments: 


Chip type               : ATmega8515
Program type            : Application
AVR Core Clock frequency: 7,372800 MHz
Memory model            : Small
External RAM size       : 0
Data Stack size         : 128
*******************************************************/

#include <mega8515.h>
#include <ev8031.h>
#include <lcd_init.h>
#include <key_scan.h>
#include <itouc.h>
#include "char_c.h"
#include "char_function.h"
// Declare your global variables here

// Standard Input/Output functions
#include <stdio.h>

int isOpen = 0;
unsigned char pass_conf[4];
unsigned char pass[4];
unsigned int test = 0;
unsigned int position = 0;
unsigned char key_btn = '';
unsigned int val = 0;

void beep_ci (void){
  int beep;
  for (beep=0; beep<400; beep++){
    PORTB.6=1;      
    delay_ms(1);
    PORTB.6=0;      
    delay_ms(1);
  }                      
}

void main(void)
{
// Declare your local variables here

// Input/Output Ports initialization
// Port A initialization
// Function: Bit7=In Bit6=In Bit5=In Bit4=In Bit3=In Bit2=In Bit1=In Bit0=In 
DDRA=(0<<DDA7) | (0<<DDA6) | (0<<DDA5) | (0<<DDA4) | (0<<DDA3) | (0<<DDA2) | (0<<DDA1) | (0<<DDA0);
// State: Bit7=T Bit6=T Bit5=T Bit4=T Bit3=T Bit2=T Bit1=T Bit0=T 
PORTA=(0<<PORTA7) | (0<<PORTA6) | (0<<PORTA5) | (0<<PORTA4) | (0<<PORTA3) | (0<<PORTA2) | (0<<PORTA1) | (0<<PORTA0);

// Port B initialization
// Function: Bit7=In Bit6=Out Bit5=In Bit4=In Bit3=In Bit2=In Bit1=In Bit0=In 
DDRB=(0<<DDB7) | (1<<DDB6) | (0<<DDB5) | (0<<DDB4) | (0<<DDB3) | (0<<DDB2) | (0<<DDB1) | (0<<DDB0);
// State: Bit7=T Bit6=0 Bit5=T Bit4=T Bit3=T Bit2=T Bit1=T Bit0=T 
PORTB=(0<<PORTB7) | (0<<PORTB6) | (0<<PORTB5) | (0<<PORTB4) | (0<<PORTB3) | (0<<PORTB2) | (0<<PORTB1) | (0<<PORTB0);

// Port C initialization
// Function: Bit7=In Bit6=In Bit5=In Bit4=In Bit3=In Bit2=In Bit1=In Bit0=In 
DDRC=(0<<DDC7) | (0<<DDC6) | (0<<DDC5) | (0<<DDC4) | (0<<DDC3) | (0<<DDC2) | (0<<DDC1) | (0<<DDC0);
// State: Bit7=T Bit6=T Bit5=T Bit4=T Bit3=T Bit2=T Bit1=T Bit0=T 
PORTC=(0<<PORTC7) | (0<<PORTC6) | (0<<PORTC5) | (0<<PORTC4) | (0<<PORTC3) | (0<<PORTC2) | (0<<PORTC1) | (0<<PORTC0);

// Port D initialization
// Function: Bit7=In Bit6=In Bit5=In Bit4=In Bit3=In Bit2=In Bit1=In Bit0=In 
DDRD=(0<<DDD7) | (0<<DDD6) | (0<<DDD5) | (0<<DDD4) | (0<<DDD3) | (0<<DDD2) | (0<<DDD1) | (0<<DDD0);
// State: Bit7=T Bit6=T Bit5=T Bit4=T Bit3=T Bit2=T Bit1=T Bit0=T 
PORTD=(0<<PORTD7) | (0<<PORTD6) | (0<<PORTD5) | (0<<PORTD4) | (0<<PORTD3) | (0<<PORTD2) | (0<<PORTD1) | (0<<PORTD0);

// Port E initialization
// Function: Bit2=In Bit1=In Bit0=In 
DDRE=(0<<DDE2) | (0<<DDE1) | (0<<DDE0);
// State: Bit2=T Bit1=T Bit0=T 
PORTE=(0<<PORTE2) | (0<<PORTE1) | (0<<PORTE0);

// Timer/Counter 0 initialization
// Clock source: System Clock
// Clock value: Timer 0 Stopped
// Mode: Normal top=0xFF
// OC0 output: Disconnected
TCCR0=(0<<PWM0) | (0<<COM01) | (0<<COM00) | (0<<CTC0) | (0<<CS02) | (0<<CS01) | (0<<CS00);
TCNT0=0x00;
OCR0=0x00;

// Timer/Counter 1 initialization
// Clock source: System Clock
// Clock value: Timer1 Stopped
// Mode: Normal top=0xFFFF
// OC1A output: Disconnected
// OC1B output: Disconnected
// Noise Canceler: Off
// Input Capture on Falling Edge
// Timer1 Overflow Interrupt: Off
// Input Capture Interrupt: Off
// Compare A Match Interrupt: Off
// Compare B Match Interrupt: Off
TCCR1A=(0<<COM1A1) | (0<<COM1A0) | (0<<COM1B1) | (0<<COM1B0) | (0<<WGM11) | (0<<WGM10);
TCCR1B=(0<<ICNC1) | (0<<ICES1) | (0<<WGM13) | (0<<WGM12) | (0<<CS12) | (0<<CS11) | (0<<CS10);
TCNT1H=0x00;
TCNT1L=0x00;
ICR1H=0x00;
ICR1L=0x00;
OCR1AH=0x00;
OCR1AL=0x00;
OCR1BH=0x00;
OCR1BL=0x00;

// Timer(s)/Counter(s) Interrupt(s) initialization
TIMSK=(0<<TOIE1) | (0<<OCIE1A) | (0<<OCIE1B) | (0<<TICIE1) | (0<<TOIE0) | (0<<OCIE0);

// External Interrupt(s) initialization
// INT0: Off
// INT1: Off
// INT2: Off
MCUCR=(0<<SRE) | (0<<SRW10) | (0<<ISC11) | (0<<ISC10) | (0<<ISC01) | (0<<ISC00);
EMCUCR=(0<<SRL2) | (0<<SRL1) | (0<<SRL0) | (0<<SRW01) | (0<<SRW00) | (0<<SRW11) | (0<<ISC2);

// USART initialization
// Communication Parameters: 8 Data, 1 Stop, No Parity
// USART Receiver: Off
// USART Transmitter: On
// USART Mode: Asynchronous
// USART Baud Rate: 9600
UCSRA=(0<<RXC) | (0<<TXC) | (0<<UDRE) | (0<<FE) | (0<<DOR) | (0<<UPE) | (0<<U2X) | (0<<MPCM);
UCSRB=(0<<RXCIE) | (0<<TXCIE) | (0<<UDRIE) | (0<<RXEN) | (1<<TXEN) | (0<<UCSZ2) | (0<<RXB8) | (0<<TXB8);
UCSRC=(1<<URSEL) | (0<<UMSEL) | (0<<UPM1) | (0<<UPM0) | (0<<USBS) | (1<<UCSZ1) | (1<<UCSZ0) | (0<<UCPOL);
UBRRH=0x00;
UBRRL=0x2F;

// Analog Comparator initialization
// Analog Comparator: Off
// The Analog Comparator's positive input is
// connected to the AIN0 pin
// The Analog Comparator's negative input is
// connected to the AIN1 pin
ACSR=(1<<ACD) | (0<<ACBG) | (0<<ACO) | (0<<ACI) | (0<<ACIE) | (0<<ACIC) | (0<<ACIS1) | (0<<ACIS0);

// SPI initialization
// SPI disabled
SPCR=(0<<SPIE) | (0<<SPE) | (0<<DORD) | (0<<MSTR) | (0<<CPOL) | (0<<CPHA) | (0<<SPR1) | (0<<SPR0);

MCUCR=0b11000000;
DDRB = 0b11000000;

*UC_REG=0x01;
pass[0] = 0x31;
pass[1] = 0x31;
pass[2] = 0x31;
pass[3] = 0x31;
lcd_init ();
putsf("5");
delay_ms(50); 
while (1)
      {     
      *LED_REG = 0x00;
      while (position != 4){
      
     *LCD_CMD=0x01;
     delay_ms(10);

     *LCD_CMD=0x80;
     delay_ms(10);
     
    *LCD_DAT=0x50;
    delay_ms(40);
    *LCD_DAT=0x41;
    delay_ms(40);
    *LCD_DAT=0x53;
    delay_ms(40);
    *LCD_DAT=0x53;
    delay_ms(40); 
    *LCD_DAT=0x57;
    delay_ms(40);
    *LCD_DAT=0x4f;
    delay_ms(40);
    *LCD_DAT=0x52;
    delay_ms(40);
    *LCD_DAT=0x44;
    delay_ms(40);
    *LCD_DAT=0x3a;
    delay_ms(40);
    delay_ms(1000);
      
      
           //key scan 
           do{ 
           
               key_btn = key_scan_char ();
               int_to_uc(key_btn,0x01);      
               if (key_btn == 30)
               {
                   key_btn = '1';
               }else if(key_btn == 23){
                   key_btn = '*';
               }else if(key_btn == 27){
                   key_btn = '7';
               }else if(key_btn == 29){
                   key_btn = '4';
               }
               
            } while (key_btn == 'f' || key_btn == 31);
           convert_addres(key_btn);  //1: ����� 1
           *LCD_DAT=key_btn;
            
            *LED_REG = 0xff;
            //delay_ms(4000);     
            
            if (position == 0){
                *LED_REG = 0b00000001;
                *LCD_DAT=key_btn;
            }else if (position == 1){
                *LED_REG = 0b00000011; 
                *LCD_DAT=key_btn;
            } else if (position == 2){
                *LED_REG = 0b00000111; 
                *LCD_DAT=key_btn;           
            }else if (position == 3){
                *LED_REG = 0b00001111; 
                *LCD_DAT=key_btn;           
            }             
           if (key_btn == '*'){ 
            if (position>0){
                pass_conf[position] = '';
                position --;
             }
           }else{ 
             pass_conf[position] = key_btn;
             position ++;
           }   
           //UpdateDisplay
           // * - callback
      }        
      position = 0;
     *LCD_CMD=0x01;
     delay_ms(40);

     *LCD_CMD=0x80;
     delay_ms(40);
     
     *LCD_DAT=pass_conf[0];
     delay_ms(40);
     *LCD_DAT=pass_conf[1]; 
     delay_ms(40);
     *LCD_DAT=pass_conf[2]; 
     delay_ms(40);
     *LCD_DAT=pass_conf[3]; 
     delay_ms(1000); 
     
     *LCD_CMD=0xc0;
     delay_ms(10);
     
     *LCD_DAT=pass[0];
     delay_ms(40);
     *LCD_DAT=pass[1]; 
     delay_ms(40);
     *LCD_DAT=pass[2]; 
     delay_ms(40);
     *LCD_DAT=pass[3]; 
     delay_ms(1000);

     *LCD_CMD=0x01;   
     delay_ms(40); 
     *LCD_CMD=0x80;
     delay_ms(40);
     delay_ms(1000);
     if (str_cmp(pass_conf,4,pass,4) == 1){
        isOpen = 1;
        putsf("1");
        delay_ms(50); 
     }else{
        isOpen --; 
     }
            
      if (isOpen == 1){
        *LCD_DAT=0x4f;
        delay_ms(40);
        *LCD_DAT=0x50;
        delay_ms(40);
        *LCD_DAT=0x45;
        delay_ms(40);
        *LCD_DAT=0x4e;
        delay_ms(40);
         
        *LCD_CMD=0xc0;
        delay_ms(40);
        *LCD_DAT=0x43;
        delay_ms(40);
        *LCD_DAT=0x4c;
        delay_ms(40);
        *LCD_DAT=0x4f;
        delay_ms(40);
        *LCD_DAT=0x53;
        delay_ms(40); 
        *LCD_DAT=0x2d;
        delay_ms(40);
        *LCD_DAT=0x23;
        delay_ms(40);
        
        *LED_REG = 0xff;
        delay_ms(1000);
        *LED_REG = 0x00;
         beep_ci();
         beep_ci();
         beep_ci();
         do{
           key_btn = key_scan_char ();
           if (key_btn == 23){
              key_btn = '*';
           }
        } while (key_btn != '*');
        isOpen = 0;
        putsf("0");
        delay_ms(50);
      }else{
        if (isOpen != -3){
        putsf("-1");
        delay_ms(50); 
        *LCD_CMD=0x01;
        delay_ms(10);

        *LCD_CMD=0x80;
         delay_ms(10);
             *LCD_CMD=0x01;
             delay_ms(10);

             *LCD_CMD=0x80;
             delay_ms(10);
            *LCD_DAT=0x45;
            delay_ms(40);
            *LCD_DAT=0x52;
            delay_ms(40);
            *LCD_DAT=0x52;
            delay_ms(40);
            *LCD_DAT=0x4f;
            delay_ms(40); 
            *LCD_DAT=0x52;
            delay_ms(1000);
        
            beep_ci();
            delay_ms(500);
            beep_ci();     
            delay_ms(500);
            beep_ci(); 
        }else if (isOpen == -3){ 
            putsf("-3");
            delay_ms(50); 
            *LCD_CMD=0x01;
             delay_ms(10);
             *LCD_CMD=0x80;
             delay_ms(10);
            *LCD_DAT=0x52;
            delay_ms(40);
            *LCD_DAT=0x55;
            delay_ms(40);
            *LCD_DAT=0x4e;
            delay_ms(40);
            *LCD_DAT=0x10;
            delay_ms(40); 
            *LCD_DAT=0x56;
            delay_ms(40);
            *LCD_DAT=0x41;
            delay_ms(40);
            *LCD_DAT=0x53;
            delay_ms(40);
            *LCD_DAT=0x41;
            delay_ms(40);
            delay_ms(1000);
            
            isOpen = 0;
            beep_ci();
            delay_ms(50);
            beep_ci();     
            delay_ms(50);
            beep_ci();  
            delay_ms(50);
            beep_ci();     
            delay_ms(50);
            beep_ci();
            delay_ms(50);
            beep_ci();     
            delay_ms(50);
            beep_ci();  
            delay_ms(50);
            beep_ci();     
            delay_ms(50);
            beep_ci(); 
        }
            
      }
      }
}
