
;CodeVisionAVR C Compiler V2.05.3 Standard
;(C) Copyright 1998-2011 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com

;Chip type                : ATmega8515
;Program type             : Application
;Clock frequency          : 7,372800 MHz
;Memory model             : Small
;Optimize for             : Size
;(s)printf features       : int, width
;(s)scanf features        : int, width
;External RAM size        : 0
;Data Stack size          : 128 byte(s)
;Heap size                : 0 byte(s)
;Promote 'char' to 'int'  : Yes
;'char' is unsigned       : Yes
;8 bit enums              : Yes
;Global 'const' stored in FLASH     : Yes
;Enhanced function parameter passing: Yes
;Enhanced core instructions         : On
;Smart register allocation          : On
;Automatic register allocation      : On

	#pragma AVRPART ADMIN PART_NAME ATmega8515
	#pragma AVRPART MEMORY PROG_FLASH 8192
	#pragma AVRPART MEMORY EEPROM 512
	#pragma AVRPART MEMORY INT_SRAM SIZE 607
	#pragma AVRPART MEMORY INT_SRAM START_ADDR 0x60

	.LISTMAC
	.EQU UDRE=0x5
	.EQU RXC=0x7
	.EQU USR=0xB
	.EQU UDR=0xC
	.EQU SPSR=0xE
	.EQU SPDR=0xF
	.EQU EERE=0x0
	.EQU EEWE=0x1
	.EQU EEMWE=0x2
	.EQU EECR=0x1C
	.EQU EEDR=0x1D
	.EQU EEARL=0x1E
	.EQU EEARH=0x1F
	.EQU WDTCR=0x21
	.EQU MCUCSR=0x34
	.EQU MCUCR=0x35
	.EQU EMCUCR=0x36
	.EQU GICR=0x3B
	.EQU SPL=0x3D
	.EQU SPH=0x3E
	.EQU SREG=0x3F

	.DEF R0X0=R0
	.DEF R0X1=R1
	.DEF R0X2=R2
	.DEF R0X3=R3
	.DEF R0X4=R4
	.DEF R0X5=R5
	.DEF R0X6=R6
	.DEF R0X7=R7
	.DEF R0X8=R8
	.DEF R0X9=R9
	.DEF R0XA=R10
	.DEF R0XB=R11
	.DEF R0XC=R12
	.DEF R0XD=R13
	.DEF R0XE=R14
	.DEF R0XF=R15
	.DEF R0X10=R16
	.DEF R0X11=R17
	.DEF R0X12=R18
	.DEF R0X13=R19
	.DEF R0X14=R20
	.DEF R0X15=R21
	.DEF R0X16=R22
	.DEF R0X17=R23
	.DEF R0X18=R24
	.DEF R0X19=R25
	.DEF R0X1A=R26
	.DEF R0X1B=R27
	.DEF R0X1C=R28
	.DEF R0X1D=R29
	.DEF R0X1E=R30
	.DEF R0X1F=R31

	.EQU __SRAM_START=0x0060
	.EQU __SRAM_END=0x025F
	.EQU __DSTACK_SIZE=0x0080
	.EQU __HEAP_SIZE=0x0000
	.EQU __CLEAR_SRAM_SIZE=__SRAM_END-__SRAM_START+1

	.MACRO __CPD1N
	CPI  R30,LOW(@0)
	LDI  R26,HIGH(@0)
	CPC  R31,R26
	LDI  R26,BYTE3(@0)
	CPC  R22,R26
	LDI  R26,BYTE4(@0)
	CPC  R23,R26
	.ENDM

	.MACRO __CPD2N
	CPI  R26,LOW(@0)
	LDI  R30,HIGH(@0)
	CPC  R27,R30
	LDI  R30,BYTE3(@0)
	CPC  R24,R30
	LDI  R30,BYTE4(@0)
	CPC  R25,R30
	.ENDM

	.MACRO __CPWRR
	CP   R@0,R@2
	CPC  R@1,R@3
	.ENDM

	.MACRO __CPWRN
	CPI  R@0,LOW(@2)
	LDI  R30,HIGH(@2)
	CPC  R@1,R30
	.ENDM

	.MACRO __ADDB1MN
	SUBI R30,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDB2MN
	SUBI R26,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDW1MN
	SUBI R30,LOW(-@0-(@1))
	SBCI R31,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW2MN
	SUBI R26,LOW(-@0-(@1))
	SBCI R27,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	SBCI R22,BYTE3(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1N
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	SBCI R22,BYTE3(-@0)
	SBCI R23,BYTE4(-@0)
	.ENDM

	.MACRO __ADDD2N
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	SBCI R24,BYTE3(-@0)
	SBCI R25,BYTE4(-@0)
	.ENDM

	.MACRO __SUBD1N
	SUBI R30,LOW(@0)
	SBCI R31,HIGH(@0)
	SBCI R22,BYTE3(@0)
	SBCI R23,BYTE4(@0)
	.ENDM

	.MACRO __SUBD2N
	SUBI R26,LOW(@0)
	SBCI R27,HIGH(@0)
	SBCI R24,BYTE3(@0)
	SBCI R25,BYTE4(@0)
	.ENDM

	.MACRO __ANDBMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ANDWMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ANDI R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ANDD1N
	ANDI R30,LOW(@0)
	ANDI R31,HIGH(@0)
	ANDI R22,BYTE3(@0)
	ANDI R23,BYTE4(@0)
	.ENDM

	.MACRO __ANDD2N
	ANDI R26,LOW(@0)
	ANDI R27,HIGH(@0)
	ANDI R24,BYTE3(@0)
	ANDI R25,BYTE4(@0)
	.ENDM

	.MACRO __ORBMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ORWMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ORI  R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ORD1N
	ORI  R30,LOW(@0)
	ORI  R31,HIGH(@0)
	ORI  R22,BYTE3(@0)
	ORI  R23,BYTE4(@0)
	.ENDM

	.MACRO __ORD2N
	ORI  R26,LOW(@0)
	ORI  R27,HIGH(@0)
	ORI  R24,BYTE3(@0)
	ORI  R25,BYTE4(@0)
	.ENDM

	.MACRO __DELAY_USB
	LDI  R24,LOW(@0)
__DELAY_USB_LOOP:
	DEC  R24
	BRNE __DELAY_USB_LOOP
	.ENDM

	.MACRO __DELAY_USW
	LDI  R24,LOW(@0)
	LDI  R25,HIGH(@0)
__DELAY_USW_LOOP:
	SBIW R24,1
	BRNE __DELAY_USW_LOOP
	.ENDM

	.MACRO __GETD1S
	LDD  R30,Y+@0
	LDD  R31,Y+@0+1
	LDD  R22,Y+@0+2
	LDD  R23,Y+@0+3
	.ENDM

	.MACRO __GETD2S
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	LDD  R24,Y+@0+2
	LDD  R25,Y+@0+3
	.ENDM

	.MACRO __PUTD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R31
	STD  Y+@0+2,R22
	STD  Y+@0+3,R23
	.ENDM

	.MACRO __PUTD2S
	STD  Y+@0,R26
	STD  Y+@0+1,R27
	STD  Y+@0+2,R24
	STD  Y+@0+3,R25
	.ENDM

	.MACRO __PUTDZ2
	STD  Z+@0,R26
	STD  Z+@0+1,R27
	STD  Z+@0+2,R24
	STD  Z+@0+3,R25
	.ENDM

	.MACRO __CLRD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R30
	STD  Y+@0+2,R30
	STD  Y+@0+3,R30
	.ENDM

	.MACRO __POINTB1MN
	LDI  R30,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW1MN
	LDI  R30,LOW(@0+(@1))
	LDI  R31,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTD1M
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __POINTW1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	LDI  R22,BYTE3(2*@0+(@1))
	LDI  R23,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTB2MN
	LDI  R26,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW2MN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTW2FN
	LDI  R26,LOW(2*@0+(@1))
	LDI  R27,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD2FN
	LDI  R26,LOW(2*@0+(@1))
	LDI  R27,HIGH(2*@0+(@1))
	LDI  R24,BYTE3(2*@0+(@1))
	LDI  R25,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTBRM
	LDI  R@0,LOW(@1)
	.ENDM

	.MACRO __POINTWRM
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __POINTBRMN
	LDI  R@0,LOW(@1+(@2))
	.ENDM

	.MACRO __POINTWRMN
	LDI  R@0,LOW(@2+(@3))
	LDI  R@1,HIGH(@2+(@3))
	.ENDM

	.MACRO __POINTWRFN
	LDI  R@0,LOW(@2*2+(@3))
	LDI  R@1,HIGH(@2*2+(@3))
	.ENDM

	.MACRO __GETD1N
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __GETD2N
	LDI  R26,LOW(@0)
	LDI  R27,HIGH(@0)
	LDI  R24,BYTE3(@0)
	LDI  R25,BYTE4(@0)
	.ENDM

	.MACRO __GETB1MN
	LDS  R30,@0+(@1)
	.ENDM

	.MACRO __GETB1HMN
	LDS  R31,@0+(@1)
	.ENDM

	.MACRO __GETW1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	.ENDM

	.MACRO __GETD1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	LDS  R22,@0+(@1)+2
	LDS  R23,@0+(@1)+3
	.ENDM

	.MACRO __GETBRMN
	LDS  R@0,@1+(@2)
	.ENDM

	.MACRO __GETWRMN
	LDS  R@0,@2+(@3)
	LDS  R@1,@2+(@3)+1
	.ENDM

	.MACRO __GETWRZ
	LDD  R@0,Z+@2
	LDD  R@1,Z+@2+1
	.ENDM

	.MACRO __GETD2Z
	LDD  R26,Z+@0
	LDD  R27,Z+@0+1
	LDD  R24,Z+@0+2
	LDD  R25,Z+@0+3
	.ENDM

	.MACRO __GETB2MN
	LDS  R26,@0+(@1)
	.ENDM

	.MACRO __GETW2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	.ENDM

	.MACRO __GETD2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	LDS  R24,@0+(@1)+2
	LDS  R25,@0+(@1)+3
	.ENDM

	.MACRO __PUTB1MN
	STS  @0+(@1),R30
	.ENDM

	.MACRO __PUTW1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	.ENDM

	.MACRO __PUTD1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	STS  @0+(@1)+2,R22
	STS  @0+(@1)+3,R23
	.ENDM

	.MACRO __PUTB1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMWRB
	.ENDM

	.MACRO __PUTW1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMWRW
	.ENDM

	.MACRO __PUTD1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMWRD
	.ENDM

	.MACRO __PUTBR0MN
	STS  @0+(@1),R0
	.ENDM

	.MACRO __PUTBMRN
	STS  @0+(@1),R@2
	.ENDM

	.MACRO __PUTWMRN
	STS  @0+(@1),R@2
	STS  @0+(@1)+1,R@3
	.ENDM

	.MACRO __PUTBZR
	STD  Z+@1,R@0
	.ENDM

	.MACRO __PUTWZR
	STD  Z+@2,R@0
	STD  Z+@2+1,R@1
	.ENDM

	.MACRO __GETW1R
	MOV  R30,R@0
	MOV  R31,R@1
	.ENDM

	.MACRO __GETW2R
	MOV  R26,R@0
	MOV  R27,R@1
	.ENDM

	.MACRO __GETWRN
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __PUTW1R
	MOV  R@0,R30
	MOV  R@1,R31
	.ENDM

	.MACRO __PUTW2R
	MOV  R@0,R26
	MOV  R@1,R27
	.ENDM

	.MACRO __ADDWRN
	SUBI R@0,LOW(-@2)
	SBCI R@1,HIGH(-@2)
	.ENDM

	.MACRO __ADDWRR
	ADD  R@0,R@2
	ADC  R@1,R@3
	.ENDM

	.MACRO __SUBWRN
	SUBI R@0,LOW(@2)
	SBCI R@1,HIGH(@2)
	.ENDM

	.MACRO __SUBWRR
	SUB  R@0,R@2
	SBC  R@1,R@3
	.ENDM

	.MACRO __ANDWRN
	ANDI R@0,LOW(@2)
	ANDI R@1,HIGH(@2)
	.ENDM

	.MACRO __ANDWRR
	AND  R@0,R@2
	AND  R@1,R@3
	.ENDM

	.MACRO __ORWRN
	ORI  R@0,LOW(@2)
	ORI  R@1,HIGH(@2)
	.ENDM

	.MACRO __ORWRR
	OR   R@0,R@2
	OR   R@1,R@3
	.ENDM

	.MACRO __EORWRR
	EOR  R@0,R@2
	EOR  R@1,R@3
	.ENDM

	.MACRO __GETWRS
	LDD  R@0,Y+@2
	LDD  R@1,Y+@2+1
	.ENDM

	.MACRO __PUTBSR
	STD  Y+@1,R@0
	.ENDM

	.MACRO __PUTWSR
	STD  Y+@2,R@0
	STD  Y+@2+1,R@1
	.ENDM

	.MACRO __MOVEWRR
	MOV  R@0,R@2
	MOV  R@1,R@3
	.ENDM

	.MACRO __INWR
	IN   R@0,@2
	IN   R@1,@2+1
	.ENDM

	.MACRO __OUTWR
	OUT  @2+1,R@1
	OUT  @2,R@0
	.ENDM

	.MACRO __CALL1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	ICALL
	.ENDM

	.MACRO __CALL1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	RCALL __GETW1PF
	ICALL
	.ENDM

	.MACRO __CALL2EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMRDW
	ICALL
	.ENDM

	.MACRO __GETW1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X
	.ENDM

	.MACRO __GETD1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X+
	LD   R22,X
	.ENDM

	.MACRO __NBST
	BST  R@0,@1
	IN   R30,SREG
	LDI  R31,0x40
	EOR  R30,R31
	OUT  SREG,R30
	.ENDM


	.MACRO __PUTB1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RNS
	MOVW R26,R@0
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	RCALL __PUTDP1
	.ENDM


	.MACRO __GETB1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R30,Z
	.ENDM

	.MACRO __GETB1HSX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	.ENDM

	.MACRO __GETW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R31,Z
	MOV  R30,R0
	.ENDM

	.MACRO __GETD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R1,Z+
	LD   R22,Z+
	LD   R23,Z
	MOVW R30,R0
	.ENDM

	.MACRO __GETB2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R26,X
	.ENDM

	.MACRO __GETW2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	.ENDM

	.MACRO __GETD2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R1,X+
	LD   R24,X+
	LD   R25,X
	MOVW R26,R0
	.ENDM

	.MACRO __GETBRSX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	LD   R@0,Z
	.ENDM

	.MACRO __GETWRSX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	LD   R@0,Z+
	LD   R@1,Z
	.ENDM

	.MACRO __GETBRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	LD   R@0,X
	.ENDM

	.MACRO __GETWRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	LD   R@0,X+
	LD   R@1,X
	.ENDM

	.MACRO __LSLW8SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	CLR  R30
	.ENDM

	.MACRO __PUTB1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __CLRW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __CLRD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R30
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __PUTB2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R26
	.ENDM

	.MACRO __PUTW2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z,R27
	.ENDM

	.MACRO __PUTD2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z+,R27
	ST   Z+,R24
	ST   Z,R25
	.ENDM

	.MACRO __PUTBSRX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	ST   Z,R@0
	.ENDM

	.MACRO __PUTWSRX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	ST   Z+,R@0
	ST   Z,R@1
	.ENDM

	.MACRO __PUTB1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __MULBRR
	MULS R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRRU
	MUL  R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRR0
	MULS R@0,R@1
	.ENDM

	.MACRO __MULBRRU0
	MUL  R@0,R@1
	.ENDM

	.MACRO __MULBNWRU
	LDI  R26,@2
	MUL  R26,R@0
	MOVW R30,R0
	MUL  R26,R@1
	ADD  R31,R0
	.ENDM

;NAME DEFINITIONS FOR GLOBAL VARIABLES ALLOCATED TO REGISTERS
	.DEF _lefti=R4
	.DEF _righti=R6
	.DEF _dotsi=R8
	.DEF _LED_REG=R10
	.DEF _LCD_CMD=R12

	.CSEG
	.ORG 0x00

;START OF CODE MARKER
__START_OF_CODE:

;INTERRUPT VECTORS
	RJMP __RESET
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00

_tbl10_G100:
	.DB  0x10,0x27,0xE8,0x3,0x64,0x0,0xA,0x0
	.DB  0x1,0x0
_tbl16_G100:
	.DB  0x0,0x10,0x0,0x1,0x10,0x0,0x1,0x0

_0x3:
	.DB  0x5,0x80
_0x4:
	.DB  0x0,0xF0
_0x5:
	.DB  0x6,0x90
_0x6:
	.DB  0x5,0x90
_0x7:
	.DB  0x3,0x90
_0x8:
	.DB  0x0,0x90
_0x9:
	.DB  0x0,0xC0
_0x107:
	.DB  0x0,0xA0,0x1,0xA0,0x4,0xA0,0x6,0xA0
	.DB  0x4,0x80
_0x0:
	.DB  0x35,0x0,0x31,0x0,0x30,0x0,0x2D,0x31
	.DB  0x0,0x2D,0x33,0x0

__GLOBAL_INI_TBL:
	.DW  0x02
	.DW  _LCD_DAT
	.DW  _0x3*2

	.DW  0x02
	.DW  _kbc0
	.DW  _0x5*2

	.DW  0x02
	.DW  _kbc1
	.DW  _0x6*2

	.DW  0x02
	.DW  _kbc2
	.DW  _0x7*2

	.DW  0x02
	.DW  _UC_REG
	.DW  _0x9*2

	.DW  0x0A
	.DW  0x04
	.DW  _0x107*2

_0xFFFFFFFF:
	.DW  0

__RESET:
	CLI
	CLR  R30
	OUT  EECR,R30

;INTERRUPT VECTORS ARE PLACED
;AT THE START OF FLASH
	LDI  R31,1
	OUT  GICR,R31
	OUT  GICR,R30
	OUT  MCUCR,R30
	OUT  EMCUCR,R30

;DISABLE WATCHDOG
	LDI  R31,0x18
	OUT  WDTCR,R31
	OUT  WDTCR,R30

;CLEAR R2-R14
	LDI  R24,(14-2)+1
	LDI  R26,2
	CLR  R27
__CLEAR_REG:
	ST   X+,R30
	DEC  R24
	BRNE __CLEAR_REG

;CLEAR SRAM
	LDI  R24,LOW(__CLEAR_SRAM_SIZE)
	LDI  R25,HIGH(__CLEAR_SRAM_SIZE)
	LDI  R26,__SRAM_START
__CLEAR_SRAM:
	ST   X+,R30
	SBIW R24,1
	BRNE __CLEAR_SRAM

;GLOBAL VARIABLES INITIALIZATION
	LDI  R30,LOW(__GLOBAL_INI_TBL*2)
	LDI  R31,HIGH(__GLOBAL_INI_TBL*2)
__GLOBAL_INI_NEXT:
	LPM  R24,Z+
	LPM  R25,Z+
	SBIW R24,0
	BREQ __GLOBAL_INI_END
	LPM  R26,Z+
	LPM  R27,Z+
	LPM  R0,Z+
	LPM  R1,Z+
	MOVW R22,R30
	MOVW R30,R0
__GLOBAL_INI_LOOP:
	LPM  R0,Z+
	ST   X+,R0
	SBIW R24,1
	BRNE __GLOBAL_INI_LOOP
	MOVW R30,R22
	RJMP __GLOBAL_INI_NEXT
__GLOBAL_INI_END:

;HARDWARE STACK POINTER INITIALIZATION
	LDI  R30,LOW(__SRAM_END-__HEAP_SIZE)
	OUT  SPL,R30
	LDI  R30,HIGH(__SRAM_END-__HEAP_SIZE)
	OUT  SPH,R30

;DATA STACK POINTER INITIALIZATION
	LDI  R28,LOW(__SRAM_START+__DSTACK_SIZE)
	LDI  R29,HIGH(__SRAM_START+__DSTACK_SIZE)

	RJMP _main

	.ESEG
	.ORG 0

	.DSEG
	.ORG 0xE0

	.CSEG
;/*******************************************************
;This program was created by the
;CodeWizardAVR V3.10 Advanced
;Automatic Program Generator
;� Copyright 1998-2014 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com
;
;Project :
;Version :
;Date    : 06.06.2016
;Author  :
;Company :
;Comments:
;
;
;Chip type               : ATmega8515
;Program type            : Application
;AVR Core Clock frequency: 7,372800 MHz
;Memory model            : Small
;External RAM size       : 0
;Data Stack size         : 128
;*******************************************************/
;
;#include <mega8515.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x20
	.SET power_ctrl_reg=mcucr
	#endif
;#include <ev8031.h>

	.DSEG
;#include <lcd_init.h>

	.CSEG
_lcd_init:
	LDI  R26,LOW(20)
	RCALL SUBOPT_0x0
	LDI  R26,LOW(10)
	RCALL SUBOPT_0x0
	LDI  R26,LOW(15)
	RCALL SUBOPT_0x0
	RCALL SUBOPT_0x1
	LDI  R30,LOW(56)
	RCALL SUBOPT_0x2
	LDI  R30,LOW(14)
	RCALL SUBOPT_0x3
	MOVW R26,R12
	LDI  R30,LOW(6)
	RCALL SUBOPT_0x3
	RET
;#include <key_scan.h>
_key_scan_char:
	LDS  R26,_kbc0
	LDS  R27,_kbc0+1
	RCALL SUBOPT_0x4
	BREQ _0x40
	RCALL SUBOPT_0x5
	BRNE _0x44
	LDI  R30,LOW(49)
	RET
	RJMP _0x43
_0x44:
	RCALL SUBOPT_0x6
	BRNE _0x45
	LDI  R30,LOW(52)
	RET
	RJMP _0x43
_0x45:
	RCALL SUBOPT_0x7
	BRNE _0x46
	LDI  R30,LOW(55)
	RET
	RJMP _0x43
_0x46:
	RCALL SUBOPT_0x8
	BRNE _0x43
	LDI  R30,LOW(42)
	RET
_0x43:
	RJMP _0x48
_0x40:
	LDS  R26,_kbc1
	LDS  R27,_kbc1+1
	RCALL SUBOPT_0x4
	BREQ _0x49
	RCALL SUBOPT_0x5
	BRNE _0x4D
	LDI  R30,LOW(50)
	RET
	RJMP _0x4C
_0x4D:
	RCALL SUBOPT_0x6
	BRNE _0x4E
	LDI  R30,LOW(53)
	RET
	RJMP _0x4C
_0x4E:
	RCALL SUBOPT_0x7
	BRNE _0x4F
	LDI  R30,LOW(56)
	RET
	RJMP _0x4C
_0x4F:
	RCALL SUBOPT_0x8
	BRNE _0x4C
	LDI  R30,LOW(48)
	RET
_0x4C:
	RJMP _0x51
_0x49:
	LDS  R26,_kbc2
	LDS  R27,_kbc2+1
	RCALL SUBOPT_0x4
	BREQ _0x52
	RCALL SUBOPT_0x5
	BRNE _0x56
	LDI  R30,LOW(51)
	RET
	RJMP _0x55
_0x56:
	RCALL SUBOPT_0x6
	BRNE _0x57
	LDI  R30,LOW(54)
	RET
	RJMP _0x55
_0x57:
	RCALL SUBOPT_0x7
	BRNE _0x58
	LDI  R30,LOW(57)
	RET
	RJMP _0x55
_0x58:
	RCALL SUBOPT_0x8
	BRNE _0x55
	LDI  R30,LOW(35)
	RET
_0x55:
	RJMP _0x5A
_0x52:
	LDI  R30,LOW(102)
	RET
_0x5A:
_0x51:
_0x48:
	RET
;#include <itouc.h>
_int_to_uc:
	ST   -Y,R26
	SBIW R28,1
	RCALL __SAVELOCR6
;	number -> Y+8
;	segment -> Y+7
;	r1 -> R16,R17
;	r2 -> R18,R19
;	ost -> R20,R21
;	out -> Y+6
	__GETWRN 20,21,0
_0x5B:
	LDD  R26,Y+8
	LDD  R27,Y+8+1
	SBIW R26,10
	BRLT _0x5D
	LDD  R30,Y+8
	LDD  R31,Y+8+1
	SBIW R30,10
	STD  Y+8,R30
	STD  Y+8+1,R31
	__ADDWRN 20,21,1
	RJMP _0x5B
_0x5D:
	MOVW R16,R20
	__GETWRS 18,19,8
	MOVW R30,R16
	SBIW R30,0
	BREQ _0xFF
	RCALL SUBOPT_0x9
	BRNE _0x62
	LDI  R30,LOW(16)
	RJMP _0x100
_0x62:
	RCALL SUBOPT_0xA
	BRNE _0x63
	LDI  R30,LOW(32)
	RJMP _0x100
_0x63:
	CPI  R30,LOW(0x3)
	LDI  R26,HIGH(0x3)
	CPC  R31,R26
	BRNE _0x64
	LDI  R30,LOW(48)
	RJMP _0x100
_0x64:
	CPI  R30,LOW(0x4)
	LDI  R26,HIGH(0x4)
	CPC  R31,R26
	BRNE _0x65
	LDI  R30,LOW(64)
	RJMP _0x100
_0x65:
	CPI  R30,LOW(0x5)
	LDI  R26,HIGH(0x5)
	CPC  R31,R26
	BRNE _0x66
	LDI  R30,LOW(80)
	RJMP _0x100
_0x66:
	CPI  R30,LOW(0x6)
	LDI  R26,HIGH(0x6)
	CPC  R31,R26
	BRNE _0x67
	LDI  R30,LOW(96)
	RJMP _0x100
_0x67:
	CPI  R30,LOW(0x7)
	LDI  R26,HIGH(0x7)
	CPC  R31,R26
	BRNE _0x68
	LDI  R30,LOW(112)
	RJMP _0x100
_0x68:
	CPI  R30,LOW(0x8)
	LDI  R26,HIGH(0x8)
	CPC  R31,R26
	BRNE _0x69
	LDI  R30,LOW(128)
	RJMP _0x100
_0x69:
	CPI  R30,LOW(0x9)
	LDI  R26,HIGH(0x9)
	CPC  R31,R26
	BRNE _0x6B
	LDI  R30,LOW(144)
	RJMP _0x100
_0x6B:
_0xFF:
	LDI  R30,LOW(0)
_0x100:
	STD  Y+6,R30
	MOVW R30,R18
	SBIW R30,0
	BREQ _0x101
	RCALL SUBOPT_0x9
	BRNE _0x70
	LDD  R30,Y+6
	ORI  R30,1
	RJMP _0x102
_0x70:
	RCALL SUBOPT_0xA
	BRNE _0x71
	LDD  R30,Y+6
	ORI  R30,2
	RJMP _0x102
_0x71:
	CPI  R30,LOW(0x3)
	LDI  R26,HIGH(0x3)
	CPC  R31,R26
	BRNE _0x72
	LDD  R30,Y+6
	ORI  R30,LOW(0x3)
	RJMP _0x102
_0x72:
	CPI  R30,LOW(0x4)
	LDI  R26,HIGH(0x4)
	CPC  R31,R26
	BRNE _0x73
	LDD  R30,Y+6
	ORI  R30,4
	RJMP _0x102
_0x73:
	CPI  R30,LOW(0x5)
	LDI  R26,HIGH(0x5)
	CPC  R31,R26
	BRNE _0x74
	LDD  R30,Y+6
	ORI  R30,LOW(0x5)
	RJMP _0x102
_0x74:
	CPI  R30,LOW(0x6)
	LDI  R26,HIGH(0x6)
	CPC  R31,R26
	BRNE _0x75
	LDD  R30,Y+6
	ORI  R30,LOW(0x6)
	RJMP _0x102
_0x75:
	CPI  R30,LOW(0x7)
	LDI  R26,HIGH(0x7)
	CPC  R31,R26
	BRNE _0x76
	LDD  R30,Y+6
	ORI  R30,LOW(0x7)
	RJMP _0x102
_0x76:
	CPI  R30,LOW(0x8)
	LDI  R26,HIGH(0x8)
	CPC  R31,R26
	BRNE _0x77
	LDD  R30,Y+6
	ORI  R30,8
	RJMP _0x102
_0x77:
	CPI  R30,LOW(0x9)
	LDI  R26,HIGH(0x9)
	CPC  R31,R26
	BRNE _0x79
	LDD  R30,Y+6
	ORI  R30,LOW(0x9)
	RJMP _0x102
_0x79:
_0x101:
	LDD  R30,Y+6
_0x102:
	STD  Y+6,R30
	MOVW R26,R8
	LDI  R30,LOW(32)
	ST   X,R30
	LDD  R30,Y+7
	RCALL SUBOPT_0xB
	RCALL SUBOPT_0x9
	BRNE _0x7D
	LDD  R30,Y+6
	MOVW R26,R4
	ST   X,R30
	RJMP _0x7C
_0x7D:
	RCALL SUBOPT_0xA
	BRNE _0x7F
	LDD  R30,Y+6
	MOVW R26,R6
	ST   X,R30
_0x7F:
_0x7C:
	RCALL __LOADLOCR6
	RJMP _0x2060001
;#include "char_c.h"
_convert_addres:
	ST   -Y,R26
	ST   -Y,R17
;	a -> Y+1
;	res -> R17
	LDI  R17,0
	LDD  R30,Y+1
	RCALL SUBOPT_0xB
	CPI  R30,LOW(0x30)
	LDI  R26,HIGH(0x30)
	CPC  R31,R26
	BRNE _0x83
	RCALL SUBOPT_0xC
	LDI  R30,LOW(48)
	ST   X,R30
	RJMP _0x82
_0x83:
	CPI  R30,LOW(0x31)
	LDI  R26,HIGH(0x31)
	CPC  R31,R26
	BRNE _0x84
	RCALL SUBOPT_0xC
	LDI  R30,LOW(49)
	ST   X,R30
	RJMP _0x82
_0x84:
	CPI  R30,LOW(0x32)
	LDI  R26,HIGH(0x32)
	CPC  R31,R26
	BRNE _0x85
	RCALL SUBOPT_0xC
	LDI  R30,LOW(50)
	ST   X,R30
	RJMP _0x82
_0x85:
	CPI  R30,LOW(0x33)
	LDI  R26,HIGH(0x33)
	CPC  R31,R26
	BRNE _0x86
	RCALL SUBOPT_0xC
	LDI  R30,LOW(51)
	ST   X,R30
	RJMP _0x82
_0x86:
	CPI  R30,LOW(0x34)
	LDI  R26,HIGH(0x34)
	CPC  R31,R26
	BRNE _0x87
	RCALL SUBOPT_0xC
	LDI  R30,LOW(52)
	ST   X,R30
	RJMP _0x82
_0x87:
	CPI  R30,LOW(0x35)
	LDI  R26,HIGH(0x35)
	CPC  R31,R26
	BRNE _0x88
	RCALL SUBOPT_0xC
	LDI  R30,LOW(53)
	ST   X,R30
	RJMP _0x82
_0x88:
	CPI  R30,LOW(0x36)
	LDI  R26,HIGH(0x36)
	CPC  R31,R26
	BRNE _0x89
	RCALL SUBOPT_0xC
	LDI  R30,LOW(54)
	ST   X,R30
	RJMP _0x82
_0x89:
	CPI  R30,LOW(0x37)
	LDI  R26,HIGH(0x37)
	CPC  R31,R26
	BRNE _0x8A
	RCALL SUBOPT_0xC
	LDI  R30,LOW(55)
	ST   X,R30
	RJMP _0x82
_0x8A:
	CPI  R30,LOW(0x38)
	LDI  R26,HIGH(0x38)
	CPC  R31,R26
	BRNE _0x8B
	RCALL SUBOPT_0xC
	LDI  R30,LOW(56)
	ST   X,R30
	RJMP _0x82
_0x8B:
	CPI  R30,LOW(0x39)
	LDI  R26,HIGH(0x39)
	CPC  R31,R26
	BRNE _0x8C
	RCALL SUBOPT_0xC
	LDI  R30,LOW(57)
	ST   X,R30
	RJMP _0x82
_0x8C:
	CPI  R30,LOW(0x3A)
	LDI  R26,HIGH(0x3A)
	CPC  R31,R26
	BRNE _0x8D
	LDI  R17,LOW(58)
	RJMP _0x82
_0x8D:
	CPI  R30,LOW(0x3B)
	LDI  R26,HIGH(0x3B)
	CPC  R31,R26
	BRNE _0x8E
	LDI  R17,LOW(59)
	RJMP _0x82
_0x8E:
	CPI  R30,LOW(0x3C)
	LDI  R26,HIGH(0x3C)
	CPC  R31,R26
	BRNE _0x8F
	LDI  R17,LOW(60)
	RJMP _0x82
_0x8F:
	CPI  R30,LOW(0x3D)
	LDI  R26,HIGH(0x3D)
	CPC  R31,R26
	BRNE _0x90
	LDI  R17,LOW(61)
	RJMP _0x82
_0x90:
	CPI  R30,LOW(0x3E)
	LDI  R26,HIGH(0x3E)
	CPC  R31,R26
	BRNE _0x91
	LDI  R17,LOW(62)
	RJMP _0x82
_0x91:
	CPI  R30,LOW(0x3F)
	LDI  R26,HIGH(0x3F)
	CPC  R31,R26
	BRNE _0x92
	LDI  R17,LOW(63)
	RJMP _0x82
_0x92:
	CPI  R30,LOW(0x41)
	LDI  R26,HIGH(0x41)
	CPC  R31,R26
	BRNE _0x93
	LDI  R17,LOW(65)
	RJMP _0x82
_0x93:
	CPI  R30,LOW(0x42)
	LDI  R26,HIGH(0x42)
	CPC  R31,R26
	BRNE _0x94
	LDI  R17,LOW(66)
	RJMP _0x82
_0x94:
	CPI  R30,LOW(0x43)
	LDI  R26,HIGH(0x43)
	CPC  R31,R26
	BRNE _0x95
	LDI  R17,LOW(67)
	RJMP _0x82
_0x95:
	CPI  R30,LOW(0x44)
	LDI  R26,HIGH(0x44)
	CPC  R31,R26
	BRNE _0x96
	LDI  R17,LOW(68)
	RJMP _0x82
_0x96:
	CPI  R30,LOW(0x45)
	LDI  R26,HIGH(0x45)
	CPC  R31,R26
	BRNE _0x97
	LDI  R17,LOW(69)
	RJMP _0x82
_0x97:
	CPI  R30,LOW(0x46)
	LDI  R26,HIGH(0x46)
	CPC  R31,R26
	BRNE _0x98
	LDI  R17,LOW(70)
	RJMP _0x82
_0x98:
	CPI  R30,LOW(0x47)
	LDI  R26,HIGH(0x47)
	CPC  R31,R26
	BRNE _0x99
	LDI  R17,LOW(71)
	RJMP _0x82
_0x99:
	CPI  R30,LOW(0x48)
	LDI  R26,HIGH(0x48)
	CPC  R31,R26
	BRNE _0x9A
	LDI  R17,LOW(72)
	RJMP _0x82
_0x9A:
	CPI  R30,LOW(0x49)
	LDI  R26,HIGH(0x49)
	CPC  R31,R26
	BRNE _0x9B
	LDI  R17,LOW(73)
	RJMP _0x82
_0x9B:
	CPI  R30,LOW(0x4A)
	LDI  R26,HIGH(0x4A)
	CPC  R31,R26
	BRNE _0x9C
	LDI  R17,LOW(74)
	RJMP _0x82
_0x9C:
	CPI  R30,LOW(0x4B)
	LDI  R26,HIGH(0x4B)
	CPC  R31,R26
	BRNE _0x9D
	LDI  R17,LOW(75)
	RJMP _0x82
_0x9D:
	CPI  R30,LOW(0x4C)
	LDI  R26,HIGH(0x4C)
	CPC  R31,R26
	BRNE _0x9E
	LDI  R17,LOW(76)
	RJMP _0x82
_0x9E:
	CPI  R30,LOW(0x4D)
	LDI  R26,HIGH(0x4D)
	CPC  R31,R26
	BRNE _0x9F
	LDI  R17,LOW(77)
	RJMP _0x82
_0x9F:
	CPI  R30,LOW(0x4E)
	LDI  R26,HIGH(0x4E)
	CPC  R31,R26
	BRNE _0xA0
	LDI  R17,LOW(78)
	RJMP _0x82
_0xA0:
	CPI  R30,LOW(0x4F)
	LDI  R26,HIGH(0x4F)
	CPC  R31,R26
	BRNE _0xA1
	LDI  R17,LOW(79)
	RJMP _0x82
_0xA1:
	CPI  R30,LOW(0x50)
	LDI  R26,HIGH(0x50)
	CPC  R31,R26
	BRNE _0xA2
	LDI  R17,LOW(80)
	RJMP _0x82
_0xA2:
	CPI  R30,LOW(0x51)
	LDI  R26,HIGH(0x51)
	CPC  R31,R26
	BRNE _0xA3
	LDI  R17,LOW(81)
	RJMP _0x82
_0xA3:
	CPI  R30,LOW(0x52)
	LDI  R26,HIGH(0x52)
	CPC  R31,R26
	BRNE _0xA4
	LDI  R17,LOW(82)
	RJMP _0x82
_0xA4:
	CPI  R30,LOW(0x53)
	LDI  R26,HIGH(0x53)
	CPC  R31,R26
	BRNE _0xA5
	LDI  R17,LOW(83)
	RJMP _0x82
_0xA5:
	CPI  R30,LOW(0x54)
	LDI  R26,HIGH(0x54)
	CPC  R31,R26
	BRNE _0xA6
	LDI  R17,LOW(84)
	RJMP _0x82
_0xA6:
	CPI  R30,LOW(0x55)
	LDI  R26,HIGH(0x55)
	CPC  R31,R26
	BRNE _0xA7
	LDI  R17,LOW(85)
	RJMP _0x82
_0xA7:
	CPI  R30,LOW(0x56)
	LDI  R26,HIGH(0x56)
	CPC  R31,R26
	BRNE _0xA8
	LDI  R17,LOW(86)
	RJMP _0x82
_0xA8:
	CPI  R30,LOW(0x57)
	LDI  R26,HIGH(0x57)
	CPC  R31,R26
	BRNE _0xA9
	LDI  R17,LOW(87)
	RJMP _0x82
_0xA9:
	CPI  R30,LOW(0x58)
	LDI  R26,HIGH(0x58)
	CPC  R31,R26
	BRNE _0xAA
	LDI  R17,LOW(88)
	RJMP _0x82
_0xAA:
	CPI  R30,LOW(0x59)
	LDI  R26,HIGH(0x59)
	CPC  R31,R26
	BRNE _0xAB
	LDI  R17,LOW(89)
	RJMP _0x82
_0xAB:
	CPI  R30,LOW(0x5A)
	LDI  R26,HIGH(0x5A)
	CPC  R31,R26
	BRNE _0xAC
	RJMP _0x103
_0xAC:
	CPI  R30,LOW(0x61)
	LDI  R26,HIGH(0x61)
	CPC  R31,R26
	BRNE _0xAD
	LDI  R17,LOW(65)
	RJMP _0x82
_0xAD:
	CPI  R30,LOW(0x62)
	LDI  R26,HIGH(0x62)
	CPC  R31,R26
	BRNE _0xAE
	LDI  R17,LOW(66)
	RJMP _0x82
_0xAE:
	CPI  R30,LOW(0x63)
	LDI  R26,HIGH(0x63)
	CPC  R31,R26
	BRNE _0xAF
	LDI  R17,LOW(67)
	RJMP _0x82
_0xAF:
	CPI  R30,LOW(0x64)
	LDI  R26,HIGH(0x64)
	CPC  R31,R26
	BRNE _0xB0
	LDI  R17,LOW(68)
	RJMP _0x82
_0xB0:
	CPI  R30,LOW(0x65)
	LDI  R26,HIGH(0x65)
	CPC  R31,R26
	BRNE _0xB1
	LDI  R17,LOW(69)
	RJMP _0x82
_0xB1:
	CPI  R30,LOW(0x66)
	LDI  R26,HIGH(0x66)
	CPC  R31,R26
	BRNE _0xB2
	LDI  R17,LOW(70)
	RJMP _0x82
_0xB2:
	CPI  R30,LOW(0x67)
	LDI  R26,HIGH(0x67)
	CPC  R31,R26
	BRNE _0xB3
	LDI  R17,LOW(71)
	RJMP _0x82
_0xB3:
	CPI  R30,LOW(0x68)
	LDI  R26,HIGH(0x68)
	CPC  R31,R26
	BRNE _0xB4
	LDI  R17,LOW(72)
	RJMP _0x82
_0xB4:
	CPI  R30,LOW(0x69)
	LDI  R26,HIGH(0x69)
	CPC  R31,R26
	BRNE _0xB5
	LDI  R17,LOW(73)
	RJMP _0x82
_0xB5:
	CPI  R30,LOW(0x6A)
	LDI  R26,HIGH(0x6A)
	CPC  R31,R26
	BRNE _0xB6
	LDI  R17,LOW(74)
	RJMP _0x82
_0xB6:
	CPI  R30,LOW(0x6B)
	LDI  R26,HIGH(0x6B)
	CPC  R31,R26
	BRNE _0xB7
	LDI  R17,LOW(75)
	RJMP _0x82
_0xB7:
	CPI  R30,LOW(0x6C)
	LDI  R26,HIGH(0x6C)
	CPC  R31,R26
	BRNE _0xB8
	LDI  R17,LOW(76)
	RJMP _0x82
_0xB8:
	CPI  R30,LOW(0x6D)
	LDI  R26,HIGH(0x6D)
	CPC  R31,R26
	BRNE _0xB9
	LDI  R17,LOW(77)
	RJMP _0x82
_0xB9:
	CPI  R30,LOW(0x6E)
	LDI  R26,HIGH(0x6E)
	CPC  R31,R26
	BRNE _0xBA
	LDI  R17,LOW(78)
	RJMP _0x82
_0xBA:
	CPI  R30,LOW(0x6F)
	LDI  R26,HIGH(0x6F)
	CPC  R31,R26
	BRNE _0xBB
	LDI  R17,LOW(79)
	RJMP _0x82
_0xBB:
	CPI  R30,LOW(0x70)
	LDI  R26,HIGH(0x70)
	CPC  R31,R26
	BRNE _0xBC
	LDI  R17,LOW(80)
	RJMP _0x82
_0xBC:
	CPI  R30,LOW(0x71)
	LDI  R26,HIGH(0x71)
	CPC  R31,R26
	BRNE _0xBD
	LDI  R17,LOW(81)
	RJMP _0x82
_0xBD:
	CPI  R30,LOW(0x72)
	LDI  R26,HIGH(0x72)
	CPC  R31,R26
	BRNE _0xBE
	LDI  R17,LOW(82)
	RJMP _0x82
_0xBE:
	CPI  R30,LOW(0x73)
	LDI  R26,HIGH(0x73)
	CPC  R31,R26
	BRNE _0xBF
	LDI  R17,LOW(83)
	RJMP _0x82
_0xBF:
	CPI  R30,LOW(0x74)
	LDI  R26,HIGH(0x74)
	CPC  R31,R26
	BRNE _0xC0
	LDI  R17,LOW(84)
	RJMP _0x82
_0xC0:
	CPI  R30,LOW(0x75)
	LDI  R26,HIGH(0x75)
	CPC  R31,R26
	BRNE _0xC1
	LDI  R17,LOW(85)
	RJMP _0x82
_0xC1:
	CPI  R30,LOW(0x76)
	LDI  R26,HIGH(0x76)
	CPC  R31,R26
	BRNE _0xC2
	LDI  R17,LOW(86)
	RJMP _0x82
_0xC2:
	CPI  R30,LOW(0x77)
	LDI  R26,HIGH(0x77)
	CPC  R31,R26
	BRNE _0xC3
	LDI  R17,LOW(87)
	RJMP _0x82
_0xC3:
	CPI  R30,LOW(0x78)
	LDI  R26,HIGH(0x78)
	CPC  R31,R26
	BRNE _0xC4
	LDI  R17,LOW(88)
	RJMP _0x82
_0xC4:
	CPI  R30,LOW(0x79)
	LDI  R26,HIGH(0x79)
	CPC  R31,R26
	BRNE _0xC5
	LDI  R17,LOW(89)
	RJMP _0x82
_0xC5:
	CPI  R30,LOW(0x7A)
	LDI  R26,HIGH(0x7A)
	CPC  R31,R26
	BRNE _0x82
_0x103:
	LDI  R17,LOW(90)
_0x82:
	LDD  R17,Y+0
	ADIW R28,2
	RET
;#include "char_function.h"
_str_cmp:
	ST   -Y,R27
	ST   -Y,R26
	RCALL __SAVELOCR2
;	*a -> Y+8
;	c1 -> Y+6
;	*b -> Y+4
;	c2 -> Y+2
;	res -> R16,R17
	__GETWRN 16,17,1
	LDD  R30,Y+2
	LDD  R31,Y+2+1
	LDD  R26,Y+6
	LDD  R27,Y+6+1
	CP   R30,R26
	CPC  R31,R27
	BREQ _0xC7
	__GETWRN 16,17,0
	RJMP _0xC8
_0xC7:
	SBIW R28,2
;	*a -> Y+10
;	c1 -> Y+8
;	*b -> Y+6
;	c2 -> Y+4
;	i -> Y+0
	LDI  R30,LOW(0)
	STD  Y+0,R30
	STD  Y+0+1,R30
_0xCA:
	LDD  R30,Y+8
	LDD  R31,Y+8+1
	LD   R26,Y
	LDD  R27,Y+1
	CP   R26,R30
	CPC  R27,R31
	BRGE _0xCB
	LD   R30,Y
	LDD  R31,Y+1
	LDD  R26,Y+10
	LDD  R27,Y+10+1
	ADD  R26,R30
	ADC  R27,R31
	LD   R0,X
	CLR  R1
	LDD  R26,Y+6
	LDD  R27,Y+6+1
	ADD  R26,R30
	ADC  R27,R31
	LD   R30,X
	MOVW R26,R0
	RCALL SUBOPT_0xB
	CP   R30,R26
	CPC  R31,R27
	BRNE _0xCC
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	RJMP _0xCD
_0xCC:
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
_0xCD:
	MOVW R16,R30
	MOV  R0,R16
	OR   R0,R17
	BREQ _0xCB
	LD   R30,Y
	LDD  R31,Y+1
	ADIW R30,1
	ST   Y,R30
	STD  Y+1,R31
	RJMP _0xCA
_0xCB:
	ADIW R28,2
_0xC8:
	MOVW R30,R16
	RCALL __LOADLOCR2
_0x2060001:
	ADIW R28,10
	RET
;// Declare your global variables here
;
;// Standard Input/Output functions
;#include <stdio.h>
;
;int isOpen = 0;
;unsigned char pass_conf[4];
;unsigned char pass[4];
;unsigned int test = 0;
;unsigned int position = 0;
;unsigned char key_btn = '';
;unsigned int val = 0;
;
;void beep_ci (void){
; 0000 002C void beep_ci (void){
_beep_ci:
; 0000 002D   int beep;
; 0000 002E   for (beep=0; beep<400; beep++){
	RCALL __SAVELOCR2
;	beep -> R16,R17
	__GETWRN 16,17,0
_0xD1:
	__CPWRN 16,17,400
	BRGE _0xD2
; 0000 002F     PORTB.6=1;
	SBI  0x18,6
; 0000 0030     delay_ms(1);
	RCALL SUBOPT_0xD
; 0000 0031     PORTB.6=0;
	CBI  0x18,6
; 0000 0032     delay_ms(1);
	RCALL SUBOPT_0xD
; 0000 0033   }
	__ADDWRN 16,17,1
	RJMP _0xD1
_0xD2:
; 0000 0034 }
	RCALL __LOADLOCR2P
	RET
;
;void main(void)
; 0000 0037 {
_main:
; 0000 0038 // Declare your local variables here
; 0000 0039 
; 0000 003A // Input/Output Ports initialization
; 0000 003B // Port A initialization
; 0000 003C // Function: Bit7=In Bit6=In Bit5=In Bit4=In Bit3=In Bit2=In Bit1=In Bit0=In
; 0000 003D DDRA=(0<<DDA7) | (0<<DDA6) | (0<<DDA5) | (0<<DDA4) | (0<<DDA3) | (0<<DDA2) | (0<<DDA1) | (0<<DDA0);
	LDI  R30,LOW(0)
	OUT  0x1A,R30
; 0000 003E // State: Bit7=T Bit6=T Bit5=T Bit4=T Bit3=T Bit2=T Bit1=T Bit0=T
; 0000 003F PORTA=(0<<PORTA7) | (0<<PORTA6) | (0<<PORTA5) | (0<<PORTA4) | (0<<PORTA3) | (0<<PORTA2) | (0<<PORTA1) | (0<<PORTA0);
	OUT  0x1B,R30
; 0000 0040 
; 0000 0041 // Port B initialization
; 0000 0042 // Function: Bit7=In Bit6=Out Bit5=In Bit4=In Bit3=In Bit2=In Bit1=In Bit0=In
; 0000 0043 DDRB=(0<<DDB7) | (1<<DDB6) | (0<<DDB5) | (0<<DDB4) | (0<<DDB3) | (0<<DDB2) | (0<<DDB1) | (0<<DDB0);
	LDI  R30,LOW(64)
	OUT  0x17,R30
; 0000 0044 // State: Bit7=T Bit6=0 Bit5=T Bit4=T Bit3=T Bit2=T Bit1=T Bit0=T
; 0000 0045 PORTB=(0<<PORTB7) | (0<<PORTB6) | (0<<PORTB5) | (0<<PORTB4) | (0<<PORTB3) | (0<<PORTB2) | (0<<PORTB1) | (0<<PORTB0);
	LDI  R30,LOW(0)
	OUT  0x18,R30
; 0000 0046 
; 0000 0047 // Port C initialization
; 0000 0048 // Function: Bit7=In Bit6=In Bit5=In Bit4=In Bit3=In Bit2=In Bit1=In Bit0=In
; 0000 0049 DDRC=(0<<DDC7) | (0<<DDC6) | (0<<DDC5) | (0<<DDC4) | (0<<DDC3) | (0<<DDC2) | (0<<DDC1) | (0<<DDC0);
	OUT  0x14,R30
; 0000 004A // State: Bit7=T Bit6=T Bit5=T Bit4=T Bit3=T Bit2=T Bit1=T Bit0=T
; 0000 004B PORTC=(0<<PORTC7) | (0<<PORTC6) | (0<<PORTC5) | (0<<PORTC4) | (0<<PORTC3) | (0<<PORTC2) | (0<<PORTC1) | (0<<PORTC0);
	OUT  0x15,R30
; 0000 004C 
; 0000 004D // Port D initialization
; 0000 004E // Function: Bit7=In Bit6=In Bit5=In Bit4=In Bit3=In Bit2=In Bit1=In Bit0=In
; 0000 004F DDRD=(0<<DDD7) | (0<<DDD6) | (0<<DDD5) | (0<<DDD4) | (0<<DDD3) | (0<<DDD2) | (0<<DDD1) | (0<<DDD0);
	OUT  0x11,R30
; 0000 0050 // State: Bit7=T Bit6=T Bit5=T Bit4=T Bit3=T Bit2=T Bit1=T Bit0=T
; 0000 0051 PORTD=(0<<PORTD7) | (0<<PORTD6) | (0<<PORTD5) | (0<<PORTD4) | (0<<PORTD3) | (0<<PORTD2) | (0<<PORTD1) | (0<<PORTD0);
	OUT  0x12,R30
; 0000 0052 
; 0000 0053 // Port E initialization
; 0000 0054 // Function: Bit2=In Bit1=In Bit0=In
; 0000 0055 DDRE=(0<<DDE2) | (0<<DDE1) | (0<<DDE0);
	OUT  0x6,R30
; 0000 0056 // State: Bit2=T Bit1=T Bit0=T
; 0000 0057 PORTE=(0<<PORTE2) | (0<<PORTE1) | (0<<PORTE0);
	OUT  0x7,R30
; 0000 0058 
; 0000 0059 // Timer/Counter 0 initialization
; 0000 005A // Clock source: System Clock
; 0000 005B // Clock value: Timer 0 Stopped
; 0000 005C // Mode: Normal top=0xFF
; 0000 005D // OC0 output: Disconnected
; 0000 005E TCCR0=(0<<PWM0) | (0<<COM01) | (0<<COM00) | (0<<CTC0) | (0<<CS02) | (0<<CS01) | (0<<CS00);
	OUT  0x33,R30
; 0000 005F TCNT0=0x00;
	OUT  0x32,R30
; 0000 0060 OCR0=0x00;
	OUT  0x31,R30
; 0000 0061 
; 0000 0062 // Timer/Counter 1 initialization
; 0000 0063 // Clock source: System Clock
; 0000 0064 // Clock value: Timer1 Stopped
; 0000 0065 // Mode: Normal top=0xFFFF
; 0000 0066 // OC1A output: Disconnected
; 0000 0067 // OC1B output: Disconnected
; 0000 0068 // Noise Canceler: Off
; 0000 0069 // Input Capture on Falling Edge
; 0000 006A // Timer1 Overflow Interrupt: Off
; 0000 006B // Input Capture Interrupt: Off
; 0000 006C // Compare A Match Interrupt: Off
; 0000 006D // Compare B Match Interrupt: Off
; 0000 006E TCCR1A=(0<<COM1A1) | (0<<COM1A0) | (0<<COM1B1) | (0<<COM1B0) | (0<<WGM11) | (0<<WGM10);
	OUT  0x2F,R30
; 0000 006F TCCR1B=(0<<ICNC1) | (0<<ICES1) | (0<<WGM13) | (0<<WGM12) | (0<<CS12) | (0<<CS11) | (0<<CS10);
	OUT  0x2E,R30
; 0000 0070 TCNT1H=0x00;
	OUT  0x2D,R30
; 0000 0071 TCNT1L=0x00;
	OUT  0x2C,R30
; 0000 0072 ICR1H=0x00;
	OUT  0x25,R30
; 0000 0073 ICR1L=0x00;
	OUT  0x24,R30
; 0000 0074 OCR1AH=0x00;
	OUT  0x2B,R30
; 0000 0075 OCR1AL=0x00;
	OUT  0x2A,R30
; 0000 0076 OCR1BH=0x00;
	OUT  0x29,R30
; 0000 0077 OCR1BL=0x00;
	OUT  0x28,R30
; 0000 0078 
; 0000 0079 // Timer(s)/Counter(s) Interrupt(s) initialization
; 0000 007A TIMSK=(0<<TOIE1) | (0<<OCIE1A) | (0<<OCIE1B) | (0<<TICIE1) | (0<<TOIE0) | (0<<OCIE0);
	OUT  0x39,R30
; 0000 007B 
; 0000 007C // External Interrupt(s) initialization
; 0000 007D // INT0: Off
; 0000 007E // INT1: Off
; 0000 007F // INT2: Off
; 0000 0080 MCUCR=(0<<SRE) | (0<<SRW10) | (0<<ISC11) | (0<<ISC10) | (0<<ISC01) | (0<<ISC00);
	OUT  0x35,R30
; 0000 0081 EMCUCR=(0<<SRL2) | (0<<SRL1) | (0<<SRL0) | (0<<SRW01) | (0<<SRW00) | (0<<SRW11) | (0<<ISC2);
	OUT  0x36,R30
; 0000 0082 
; 0000 0083 // USART initialization
; 0000 0084 // Communication Parameters: 8 Data, 1 Stop, No Parity
; 0000 0085 // USART Receiver: Off
; 0000 0086 // USART Transmitter: On
; 0000 0087 // USART Mode: Asynchronous
; 0000 0088 // USART Baud Rate: 9600
; 0000 0089 UCSRA=(0<<RXC) | (0<<TXC) | (0<<UDRE) | (0<<FE) | (0<<DOR) | (0<<UPE) | (0<<U2X) | (0<<MPCM);
	OUT  0xB,R30
; 0000 008A UCSRB=(0<<RXCIE) | (0<<TXCIE) | (0<<UDRIE) | (0<<RXEN) | (1<<TXEN) | (0<<UCSZ2) | (0<<RXB8) | (0<<TXB8);
	LDI  R30,LOW(8)
	OUT  0xA,R30
; 0000 008B UCSRC=(1<<URSEL) | (0<<UMSEL) | (0<<UPM1) | (0<<UPM0) | (0<<USBS) | (1<<UCSZ1) | (1<<UCSZ0) | (0<<UCPOL);
	LDI  R30,LOW(134)
	OUT  0x20,R30
; 0000 008C UBRRH=0x00;
	LDI  R30,LOW(0)
	OUT  0x20,R30
; 0000 008D UBRRL=0x2F;
	LDI  R30,LOW(47)
	OUT  0x9,R30
; 0000 008E 
; 0000 008F // Analog Comparator initialization
; 0000 0090 // Analog Comparator: Off
; 0000 0091 // The Analog Comparator's positive input is
; 0000 0092 // connected to the AIN0 pin
; 0000 0093 // The Analog Comparator's negative input is
; 0000 0094 // connected to the AIN1 pin
; 0000 0095 ACSR=(1<<ACD) | (0<<ACBG) | (0<<ACO) | (0<<ACI) | (0<<ACIE) | (0<<ACIC) | (0<<ACIS1) | (0<<ACIS0);
	LDI  R30,LOW(128)
	OUT  0x8,R30
; 0000 0096 
; 0000 0097 // SPI initialization
; 0000 0098 // SPI disabled
; 0000 0099 SPCR=(0<<SPIE) | (0<<SPE) | (0<<DORD) | (0<<MSTR) | (0<<CPOL) | (0<<CPHA) | (0<<SPR1) | (0<<SPR0);
	LDI  R30,LOW(0)
	OUT  0xD,R30
; 0000 009A 
; 0000 009B MCUCR=0b11000000;
	LDI  R30,LOW(192)
	OUT  0x35,R30
; 0000 009C DDRB = 0b11000000;
	OUT  0x17,R30
; 0000 009D 
; 0000 009E *UC_REG=0x01;
	LDS  R26,_UC_REG
	LDS  R27,_UC_REG+1
	RCALL SUBOPT_0xE
; 0000 009F pass[0] = 0x31;
	LDI  R30,LOW(49)
	STS  _pass,R30
; 0000 00A0 pass[1] = 0x31;
	__PUTB1MN _pass,1
; 0000 00A1 pass[2] = 0x31;
	__PUTB1MN _pass,2
; 0000 00A2 pass[3] = 0x31;
	__PUTB1MN _pass,3
; 0000 00A3 lcd_init ();
	RCALL _lcd_init
; 0000 00A4 putsf("5");
	__POINTW2FN _0x0,0
	RCALL SUBOPT_0xF
; 0000 00A5 delay_ms(50);
; 0000 00A6 while (1)
_0xD7:
; 0000 00A7       {
; 0000 00A8       *LED_REG = 0x00;
	MOVW R26,R10
	LDI  R30,LOW(0)
	ST   X,R30
; 0000 00A9       while (position != 4){
_0xDA:
	RCALL SUBOPT_0x10
	SBIW R26,4
	BRNE PC+2
	RJMP _0xDC
; 0000 00AA 
; 0000 00AB      *LCD_CMD=0x01;
	RCALL SUBOPT_0x11
; 0000 00AC      delay_ms(10);
	RCALL SUBOPT_0x12
; 0000 00AD 
; 0000 00AE      *LCD_CMD=0x80;
	RCALL SUBOPT_0x13
; 0000 00AF      delay_ms(10);
; 0000 00B0 
; 0000 00B1     *LCD_DAT=0x50;
	RCALL SUBOPT_0x14
; 0000 00B2     delay_ms(40);
; 0000 00B3     *LCD_DAT=0x41;
	RCALL SUBOPT_0x15
; 0000 00B4     delay_ms(40);
; 0000 00B5     *LCD_DAT=0x53;
; 0000 00B6     delay_ms(40);
; 0000 00B7     *LCD_DAT=0x53;
	RCALL SUBOPT_0x16
; 0000 00B8     delay_ms(40);
; 0000 00B9     *LCD_DAT=0x57;
	LDI  R30,LOW(87)
	RCALL SUBOPT_0x17
; 0000 00BA     delay_ms(40);
; 0000 00BB     *LCD_DAT=0x4f;
	LDI  R30,LOW(79)
	RCALL SUBOPT_0x17
; 0000 00BC     delay_ms(40);
; 0000 00BD     *LCD_DAT=0x52;
	RCALL SUBOPT_0x18
; 0000 00BE     delay_ms(40);
; 0000 00BF     *LCD_DAT=0x44;
	LDI  R30,LOW(68)
	RCALL SUBOPT_0x17
; 0000 00C0     delay_ms(40);
; 0000 00C1     *LCD_DAT=0x3a;
	LDI  R30,LOW(58)
	RCALL SUBOPT_0x19
; 0000 00C2     delay_ms(40);
; 0000 00C3     delay_ms(1000);
	RCALL SUBOPT_0x1A
; 0000 00C4 
; 0000 00C5 
; 0000 00C6            //key scan
; 0000 00C7            do{
_0xDE:
; 0000 00C8 
; 0000 00C9                key_btn = key_scan_char ();
	RCALL _key_scan_char
	STS  _key_btn,R30
; 0000 00CA                int_to_uc(key_btn,0x01);
	RCALL SUBOPT_0xB
	RCALL SUBOPT_0x1B
	LDI  R26,LOW(1)
	RCALL _int_to_uc
; 0000 00CB                if (key_btn == 30)
	RCALL SUBOPT_0x1C
	CPI  R26,LOW(0x1E)
	BRNE _0xE0
; 0000 00CC                {
; 0000 00CD                    key_btn = '1';
	LDI  R30,LOW(49)
	RJMP _0x104
; 0000 00CE                }else if(key_btn == 23){
_0xE0:
	RCALL SUBOPT_0x1C
	CPI  R26,LOW(0x17)
	BRNE _0xE2
; 0000 00CF                    key_btn = '*';
	LDI  R30,LOW(42)
	RJMP _0x104
; 0000 00D0                }else if(key_btn == 27){
_0xE2:
	RCALL SUBOPT_0x1C
	CPI  R26,LOW(0x1B)
	BRNE _0xE4
; 0000 00D1                    key_btn = '7';
	LDI  R30,LOW(55)
	RJMP _0x104
; 0000 00D2                }else if(key_btn == 29){
_0xE4:
	RCALL SUBOPT_0x1C
	CPI  R26,LOW(0x1D)
	BRNE _0xE6
; 0000 00D3                    key_btn = '4';
	LDI  R30,LOW(52)
_0x104:
	STS  _key_btn,R30
; 0000 00D4                }
; 0000 00D5 
; 0000 00D6             } while (key_btn == 'f' || key_btn == 31);
_0xE6:
	RCALL SUBOPT_0x1C
	CPI  R26,LOW(0x66)
	BREQ _0xE7
	RCALL SUBOPT_0x1C
	CPI  R26,LOW(0x1F)
	BRNE _0xDF
_0xE7:
	RJMP _0xDE
_0xDF:
; 0000 00D7            convert_addres(key_btn);  //1: ����� 1
	RCALL SUBOPT_0x1C
	RCALL _convert_addres
; 0000 00D8            *LCD_DAT=key_btn;
	RCALL SUBOPT_0x1D
; 0000 00D9 
; 0000 00DA             *LED_REG = 0xff;
	MOVW R26,R10
	LDI  R30,LOW(255)
	ST   X,R30
; 0000 00DB             //delay_ms(4000);
; 0000 00DC 
; 0000 00DD             if (position == 0){
	RCALL SUBOPT_0x1E
	SBIW R30,0
	BRNE _0xE9
; 0000 00DE                 *LED_REG = 0b00000001;
	MOVW R26,R10
	LDI  R30,LOW(1)
	RJMP _0x105
; 0000 00DF                 *LCD_DAT=key_btn;
; 0000 00E0             }else if (position == 1){
_0xE9:
	RCALL SUBOPT_0x10
	SBIW R26,1
	BRNE _0xEB
; 0000 00E1                 *LED_REG = 0b00000011;
	MOVW R26,R10
	LDI  R30,LOW(3)
	RJMP _0x105
; 0000 00E2                 *LCD_DAT=key_btn;
; 0000 00E3             } else if (position == 2){
_0xEB:
	RCALL SUBOPT_0x10
	SBIW R26,2
	BRNE _0xED
; 0000 00E4                 *LED_REG = 0b00000111;
	MOVW R26,R10
	LDI  R30,LOW(7)
	RJMP _0x105
; 0000 00E5                 *LCD_DAT=key_btn;
; 0000 00E6             }else if (position == 3){
_0xED:
	RCALL SUBOPT_0x10
	SBIW R26,3
	BRNE _0xEF
; 0000 00E7                 *LED_REG = 0b00001111;
	MOVW R26,R10
	LDI  R30,LOW(15)
_0x105:
	ST   X,R30
; 0000 00E8                 *LCD_DAT=key_btn;
	RCALL SUBOPT_0x1D
; 0000 00E9             }
; 0000 00EA            if (key_btn == '*'){
_0xEF:
	RCALL SUBOPT_0x1C
	CPI  R26,LOW(0x2A)
	BRNE _0xF0
; 0000 00EB             if (position>0){
	RCALL SUBOPT_0x10
	RCALL __CPW02
	BRSH _0xF1
; 0000 00EC                 pass_conf[position] = '';
	RCALL SUBOPT_0x1E
	SUBI R30,LOW(-_pass_conf)
	SBCI R31,HIGH(-_pass_conf)
	LDI  R26,LOW(0)
	RCALL SUBOPT_0x1F
; 0000 00ED                 position --;
	SBIW R30,1
	ST   -X,R31
	ST   -X,R30
; 0000 00EE              }
; 0000 00EF            }else{
_0xF1:
	RJMP _0xF2
_0xF0:
; 0000 00F0              pass_conf[position] = key_btn;
	RCALL SUBOPT_0x1E
	SUBI R30,LOW(-_pass_conf)
	SBCI R31,HIGH(-_pass_conf)
	RCALL SUBOPT_0x1C
	RCALL SUBOPT_0x1F
; 0000 00F1              position ++;
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
; 0000 00F2            }
_0xF2:
; 0000 00F3            //UpdateDisplay
; 0000 00F4            // * - callback
; 0000 00F5       }
	RJMP _0xDA
_0xDC:
; 0000 00F6       position = 0;
	LDI  R30,LOW(0)
	STS  _position,R30
	STS  _position+1,R30
; 0000 00F7      *LCD_CMD=0x01;
	MOVW R26,R12
	LDI  R30,LOW(1)
	RCALL SUBOPT_0x2
; 0000 00F8      delay_ms(40);
; 0000 00F9 
; 0000 00FA      *LCD_CMD=0x80;
	LDI  R30,LOW(128)
	RCALL SUBOPT_0x19
; 0000 00FB      delay_ms(40);
; 0000 00FC 
; 0000 00FD      *LCD_DAT=pass_conf[0];
	LDS  R30,_pass_conf
	RCALL SUBOPT_0x20
; 0000 00FE      delay_ms(40);
; 0000 00FF      *LCD_DAT=pass_conf[1];
	__GETB1MN _pass_conf,1
	RCALL SUBOPT_0x20
; 0000 0100      delay_ms(40);
; 0000 0101      *LCD_DAT=pass_conf[2];
	__GETB1MN _pass_conf,2
	RCALL SUBOPT_0x20
; 0000 0102      delay_ms(40);
; 0000 0103      *LCD_DAT=pass_conf[3];
	__GETB1MN _pass_conf,3
	RCALL SUBOPT_0xC
	RCALL SUBOPT_0x21
; 0000 0104      delay_ms(1000);
; 0000 0105 
; 0000 0106      *LCD_CMD=0xc0;
	MOVW R26,R12
	LDI  R30,LOW(192)
	ST   X,R30
; 0000 0107      delay_ms(10);
	RCALL SUBOPT_0x12
; 0000 0108 
; 0000 0109      *LCD_DAT=pass[0];
	LDS  R30,_pass
	RCALL SUBOPT_0x20
; 0000 010A      delay_ms(40);
; 0000 010B      *LCD_DAT=pass[1];
	__GETB1MN _pass,1
	RCALL SUBOPT_0x20
; 0000 010C      delay_ms(40);
; 0000 010D      *LCD_DAT=pass[2];
	__GETB1MN _pass,2
	RCALL SUBOPT_0x20
; 0000 010E      delay_ms(40);
; 0000 010F      *LCD_DAT=pass[3];
	__GETB1MN _pass,3
	RCALL SUBOPT_0xC
	RCALL SUBOPT_0x21
; 0000 0110      delay_ms(1000);
; 0000 0111 
; 0000 0112      *LCD_CMD=0x01;
	MOVW R26,R12
	LDI  R30,LOW(1)
	RCALL SUBOPT_0x2
; 0000 0113      delay_ms(40);
; 0000 0114      *LCD_CMD=0x80;
	LDI  R30,LOW(128)
	RCALL SUBOPT_0x19
; 0000 0115      delay_ms(40);
; 0000 0116      delay_ms(1000);
	RCALL SUBOPT_0x1A
; 0000 0117      if (str_cmp(pass_conf,4,pass,4) == 1){
	LDI  R30,LOW(_pass_conf)
	LDI  R31,HIGH(_pass_conf)
	RCALL SUBOPT_0x1B
	LDI  R30,LOW(4)
	LDI  R31,HIGH(4)
	RCALL SUBOPT_0x1B
	LDI  R30,LOW(_pass)
	LDI  R31,HIGH(_pass)
	RCALL SUBOPT_0x1B
	LDI  R26,LOW(4)
	RCALL SUBOPT_0x22
	RCALL _str_cmp
	RCALL SUBOPT_0x9
	BRNE _0xF3
; 0000 0118         isOpen = 1;
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	STS  _isOpen,R30
	STS  _isOpen+1,R31
; 0000 0119         putsf("1");
	__POINTW2FN _0x0,2
	RCALL SUBOPT_0xF
; 0000 011A         delay_ms(50);
; 0000 011B      }else{
	RJMP _0xF4
_0xF3:
; 0000 011C         isOpen --;
	LDI  R26,LOW(_isOpen)
	LDI  R27,HIGH(_isOpen)
	LD   R30,X+
	LD   R31,X+
	SBIW R30,1
	ST   -X,R31
	ST   -X,R30
; 0000 011D      }
_0xF4:
; 0000 011E 
; 0000 011F       if (isOpen == 1){
	RCALL SUBOPT_0x23
	SBIW R26,1
	BRNE _0xF5
; 0000 0120         *LCD_DAT=0x4f;
	RCALL SUBOPT_0xC
	LDI  R30,LOW(79)
	RCALL SUBOPT_0x19
; 0000 0121         delay_ms(40);
; 0000 0122         *LCD_DAT=0x50;
	RCALL SUBOPT_0x14
; 0000 0123         delay_ms(40);
; 0000 0124         *LCD_DAT=0x45;
	LDI  R30,LOW(69)
	RCALL SUBOPT_0x17
; 0000 0125         delay_ms(40);
; 0000 0126         *LCD_DAT=0x4e;
	LDI  R30,LOW(78)
	RCALL SUBOPT_0x2
; 0000 0127         delay_ms(40);
; 0000 0128 
; 0000 0129         *LCD_CMD=0xc0;
	LDI  R30,LOW(192)
	RCALL SUBOPT_0x17
; 0000 012A         delay_ms(40);
; 0000 012B         *LCD_DAT=0x43;
	LDI  R30,LOW(67)
	RCALL SUBOPT_0x17
; 0000 012C         delay_ms(40);
; 0000 012D         *LCD_DAT=0x4c;
	LDI  R30,LOW(76)
	RCALL SUBOPT_0x17
; 0000 012E         delay_ms(40);
; 0000 012F         *LCD_DAT=0x4f;
	LDI  R30,LOW(79)
	RCALL SUBOPT_0x17
; 0000 0130         delay_ms(40);
; 0000 0131         *LCD_DAT=0x53;
	RCALL SUBOPT_0x16
; 0000 0132         delay_ms(40);
; 0000 0133         *LCD_DAT=0x2d;
	LDI  R30,LOW(45)
	RCALL SUBOPT_0x17
; 0000 0134         delay_ms(40);
; 0000 0135         *LCD_DAT=0x23;
	LDI  R30,LOW(35)
	RCALL SUBOPT_0x19
; 0000 0136         delay_ms(40);
; 0000 0137 
; 0000 0138         *LED_REG = 0xff;
	MOVW R26,R10
	LDI  R30,LOW(255)
	RCALL SUBOPT_0x21
; 0000 0139         delay_ms(1000);
; 0000 013A         *LED_REG = 0x00;
	MOVW R26,R10
	LDI  R30,LOW(0)
	ST   X,R30
; 0000 013B          beep_ci();
	RCALL _beep_ci
; 0000 013C          beep_ci();
	RCALL _beep_ci
; 0000 013D          beep_ci();
	RCALL _beep_ci
; 0000 013E          do{
_0xF7:
; 0000 013F            key_btn = key_scan_char ();
	RCALL _key_scan_char
	STS  _key_btn,R30
; 0000 0140            if (key_btn == 23){
	RCALL SUBOPT_0x1C
	CPI  R26,LOW(0x17)
	BRNE _0xF9
; 0000 0141               key_btn = '*';
	LDI  R30,LOW(42)
	STS  _key_btn,R30
; 0000 0142            }
; 0000 0143         } while (key_btn != '*');
_0xF9:
	RCALL SUBOPT_0x1C
	CPI  R26,LOW(0x2A)
	BRNE _0xF7
; 0000 0144         isOpen = 0;
	RCALL SUBOPT_0x24
; 0000 0145         putsf("0");
	__POINTW2FN _0x0,4
	RCALL SUBOPT_0xF
; 0000 0146         delay_ms(50);
; 0000 0147       }else{
	RJMP _0xFA
_0xF5:
; 0000 0148         if (isOpen != -3){
	RCALL SUBOPT_0x25
	BREQ _0xFB
; 0000 0149         putsf("-1");
	__POINTW2FN _0x0,6
	RCALL SUBOPT_0xF
; 0000 014A         delay_ms(50);
; 0000 014B         *LCD_CMD=0x01;
	RCALL SUBOPT_0x11
; 0000 014C         delay_ms(10);
	RCALL SUBOPT_0x12
; 0000 014D 
; 0000 014E         *LCD_CMD=0x80;
	RCALL SUBOPT_0x13
; 0000 014F          delay_ms(10);
; 0000 0150              *LCD_CMD=0x01;
	RCALL SUBOPT_0x11
; 0000 0151              delay_ms(10);
	RCALL SUBOPT_0x12
; 0000 0152 
; 0000 0153              *LCD_CMD=0x80;
	RCALL SUBOPT_0x13
; 0000 0154              delay_ms(10);
; 0000 0155             *LCD_DAT=0x45;
	RCALL SUBOPT_0xC
	LDI  R30,LOW(69)
	RCALL SUBOPT_0x17
; 0000 0156             delay_ms(40);
; 0000 0157             *LCD_DAT=0x52;
	RCALL SUBOPT_0x18
; 0000 0158             delay_ms(40);
; 0000 0159             *LCD_DAT=0x52;
	RCALL SUBOPT_0x18
; 0000 015A             delay_ms(40);
; 0000 015B             *LCD_DAT=0x4f;
	LDI  R30,LOW(79)
	RCALL SUBOPT_0x17
; 0000 015C             delay_ms(40);
; 0000 015D             *LCD_DAT=0x52;
	LDI  R30,LOW(82)
	RCALL SUBOPT_0x21
; 0000 015E             delay_ms(1000);
; 0000 015F 
; 0000 0160             beep_ci();
	RCALL _beep_ci
; 0000 0161             delay_ms(500);
	LDI  R26,LOW(500)
	LDI  R27,HIGH(500)
	RCALL SUBOPT_0x26
; 0000 0162             beep_ci();
; 0000 0163             delay_ms(500);
	LDI  R26,LOW(500)
	LDI  R27,HIGH(500)
	RJMP _0x106
; 0000 0164             beep_ci();
; 0000 0165         }else if (isOpen == -3){
_0xFB:
	RCALL SUBOPT_0x25
	BRNE _0xFD
; 0000 0166             putsf("-3");
	__POINTW2FN _0x0,9
	RCALL SUBOPT_0xF
; 0000 0167             delay_ms(50);
; 0000 0168             *LCD_CMD=0x01;
	RCALL SUBOPT_0x11
; 0000 0169              delay_ms(10);
	RCALL SUBOPT_0x12
; 0000 016A              *LCD_CMD=0x80;
	RCALL SUBOPT_0x13
; 0000 016B              delay_ms(10);
; 0000 016C             *LCD_DAT=0x52;
	RCALL SUBOPT_0xC
	RCALL SUBOPT_0x18
; 0000 016D             delay_ms(40);
; 0000 016E             *LCD_DAT=0x55;
	LDI  R30,LOW(85)
	RCALL SUBOPT_0x17
; 0000 016F             delay_ms(40);
; 0000 0170             *LCD_DAT=0x4e;
	LDI  R30,LOW(78)
	RCALL SUBOPT_0x17
; 0000 0171             delay_ms(40);
; 0000 0172             *LCD_DAT=0x10;
	LDI  R30,LOW(16)
	RCALL SUBOPT_0x17
; 0000 0173             delay_ms(40);
; 0000 0174             *LCD_DAT=0x56;
	LDI  R30,LOW(86)
	RCALL SUBOPT_0x17
; 0000 0175             delay_ms(40);
; 0000 0176             *LCD_DAT=0x41;
	RCALL SUBOPT_0x15
; 0000 0177             delay_ms(40);
; 0000 0178             *LCD_DAT=0x53;
; 0000 0179             delay_ms(40);
; 0000 017A             *LCD_DAT=0x41;
	LDI  R30,LOW(65)
	RCALL SUBOPT_0x19
; 0000 017B             delay_ms(40);
; 0000 017C             delay_ms(1000);
	RCALL SUBOPT_0x1A
; 0000 017D 
; 0000 017E             isOpen = 0;
	RCALL SUBOPT_0x24
; 0000 017F             beep_ci();
	RCALL _beep_ci
; 0000 0180             delay_ms(50);
	RCALL SUBOPT_0x27
; 0000 0181             beep_ci();
; 0000 0182             delay_ms(50);
	RCALL SUBOPT_0x27
; 0000 0183             beep_ci();
; 0000 0184             delay_ms(50);
	RCALL SUBOPT_0x27
; 0000 0185             beep_ci();
; 0000 0186             delay_ms(50);
	RCALL SUBOPT_0x27
; 0000 0187             beep_ci();
; 0000 0188             delay_ms(50);
	RCALL SUBOPT_0x27
; 0000 0189             beep_ci();
; 0000 018A             delay_ms(50);
	RCALL SUBOPT_0x27
; 0000 018B             beep_ci();
; 0000 018C             delay_ms(50);
	RCALL SUBOPT_0x27
; 0000 018D             beep_ci();
; 0000 018E             delay_ms(50);
	LDI  R26,LOW(50)
	RCALL SUBOPT_0x22
_0x106:
	RCALL _delay_ms
; 0000 018F             beep_ci();
	RCALL _beep_ci
; 0000 0190         }
; 0000 0191 
; 0000 0192       }
_0xFD:
_0xFA:
; 0000 0193       }
	RJMP _0xD7
; 0000 0194 }
_0xFE:
	NOP
	RJMP _0xFE
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x20
	.SET power_ctrl_reg=mcucr
	#endif

	.CSEG
_putchar:
	ST   -Y,R26
putchar0:
     sbis usr,udre
     rjmp putchar0
     ld   r30,y
     out  udr,r30
	ADIW R28,1
	RET
_putsf:
	ST   -Y,R27
	ST   -Y,R26
	ST   -Y,R17
_0x2000006:
	LDD  R30,Y+1
	LDD  R31,Y+1+1
	ADIW R30,1
	STD  Y+1,R30
	STD  Y+1+1,R31
	SBIW R30,1
	LPM  R30,Z
	MOV  R17,R30
	CPI  R30,0
	BREQ _0x2000008
	MOV  R26,R17
	RCALL _putchar
	RJMP _0x2000006
_0x2000008:
	LDI  R26,LOW(10)
	RCALL _putchar
	LDD  R17,Y+0
	ADIW R28,3
	RET

	.CSEG

	.CSEG

	.DSEG
_LCD_DAT:
	.BYTE 0x2
_kbc0:
	.BYTE 0x2
_kbc1:
	.BYTE 0x2
_kbc2:
	.BYTE 0x2
_UC_REG:
	.BYTE 0x2
_key:
	.BYTE 0x1
_isOpen:
	.BYTE 0x2
_pass_conf:
	.BYTE 0x4
_pass:
	.BYTE 0x4
_position:
	.BYTE 0x2
_key_btn:
	.BYTE 0x1

	.CSEG
;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x0:
	LDI  R27,0
	RCALL _delay_ms
	MOVW R26,R12
	LDI  R30,LOW(48)
	ST   X,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:18 WORDS
SUBOPT_0x1:
	LDI  R26,LOW(40)
	LDI  R27,0
	RCALL _delay_ms
	MOVW R26,R12
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x2:
	ST   X,R30
	RJMP SUBOPT_0x1

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x3:
	ST   X,R30
	LDI  R26,LOW(5)
	LDI  R27,0
	RJMP _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:8 WORDS
SUBOPT_0x4:
	LD   R30,X
	STS  _key,R30
	LDS  R26,_key
	CPI  R26,LOW(0xFF)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:12 WORDS
SUBOPT_0x5:
	LDS  R30,_key
	LDI  R31,0
	CPI  R30,LOW(0xFE)
	LDI  R26,HIGH(0xFE)
	CPC  R31,R26
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x6:
	CPI  R30,LOW(0xFD)
	LDI  R26,HIGH(0xFD)
	CPC  R31,R26
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x7:
	CPI  R30,LOW(0xFB)
	LDI  R26,HIGH(0xFB)
	CPC  R31,R26
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x8:
	CPI  R30,LOW(0xF7)
	LDI  R26,HIGH(0xF7)
	CPC  R31,R26
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x9:
	CPI  R30,LOW(0x1)
	LDI  R26,HIGH(0x1)
	CPC  R31,R26
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0xA:
	CPI  R30,LOW(0x2)
	LDI  R26,HIGH(0x2)
	CPC  R31,R26
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0xB:
	LDI  R31,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 52 TIMES, CODE SIZE REDUCTION:151 WORDS
SUBOPT_0xC:
	LDS  R26,_LCD_DAT
	LDS  R27,_LCD_DAT+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0xD:
	LDI  R26,LOW(1)
	LDI  R27,0
	RJMP _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0xE:
	LDI  R30,LOW(1)
	ST   X,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:18 WORDS
SUBOPT_0xF:
	RCALL _putsf
	LDI  R26,LOW(50)
	LDI  R27,0
	RJMP _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x10:
	LDS  R26,_position
	LDS  R27,_position+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x11:
	MOVW R26,R12
	RJMP SUBOPT_0xE

;OPTIMIZER ADDED SUBROUTINE, CALLED 9 TIMES, CODE SIZE REDUCTION:30 WORDS
SUBOPT_0x12:
	LDI  R26,LOW(10)
	LDI  R27,0
	RJMP _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x13:
	MOVW R26,R12
	LDI  R30,LOW(128)
	ST   X,R30
	RJMP SUBOPT_0x12

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x14:
	RCALL SUBOPT_0xC
	LDI  R30,LOW(80)
	ST   X,R30
	LDI  R26,LOW(40)
	LDI  R27,0
	RCALL _delay_ms
	RJMP SUBOPT_0xC

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:13 WORDS
SUBOPT_0x15:
	LDI  R30,LOW(65)
	ST   X,R30
	LDI  R26,LOW(40)
	LDI  R27,0
	RCALL _delay_ms
	RCALL SUBOPT_0xC
	LDI  R30,LOW(83)
	ST   X,R30
	LDI  R26,LOW(40)
	LDI  R27,0
	RCALL _delay_ms
	RJMP SUBOPT_0xC

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x16:
	LDI  R30,LOW(83)
	ST   X,R30
	LDI  R26,LOW(40)
	LDI  R27,0
	RCALL _delay_ms
	RJMP SUBOPT_0xC

;OPTIMIZER ADDED SUBROUTINE, CALLED 19 TIMES, CODE SIZE REDUCTION:106 WORDS
SUBOPT_0x17:
	ST   X,R30
	LDI  R26,LOW(40)
	LDI  R27,0
	RCALL _delay_ms
	RJMP SUBOPT_0xC

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x18:
	LDI  R30,LOW(82)
	RJMP SUBOPT_0x17

;OPTIMIZER ADDED SUBROUTINE, CALLED 12 TIMES, CODE SIZE REDUCTION:53 WORDS
SUBOPT_0x19:
	ST   X,R30
	LDI  R26,LOW(40)
	LDI  R27,0
	RJMP _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x1A:
	LDI  R26,LOW(1000)
	LDI  R27,HIGH(1000)
	RJMP _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x1B:
	ST   -Y,R31
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 11 TIMES, CODE SIZE REDUCTION:8 WORDS
SUBOPT_0x1C:
	LDS  R26,_key_btn
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x1D:
	LDS  R30,_key_btn
	RCALL SUBOPT_0xC
	ST   X,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x1E:
	LDS  R30,_position
	LDS  R31,_position+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x1F:
	STD  Z+0,R26
	LDI  R26,LOW(_position)
	LDI  R27,HIGH(_position)
	LD   R30,X+
	LD   R31,X+
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x20:
	RCALL SUBOPT_0xC
	RJMP SUBOPT_0x19

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x21:
	ST   X,R30
	RJMP SUBOPT_0x1A

;OPTIMIZER ADDED SUBROUTINE, CALLED 9 TIMES, CODE SIZE REDUCTION:14 WORDS
SUBOPT_0x22:
	LDI  R27,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x23:
	LDS  R26,_isOpen
	LDS  R27,_isOpen+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x24:
	LDI  R30,LOW(0)
	STS  _isOpen,R30
	STS  _isOpen+1,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x25:
	RCALL SUBOPT_0x23
	CPI  R26,LOW(0xFFFD)
	LDI  R30,HIGH(0xFFFD)
	CPC  R27,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x26:
	RCALL _delay_ms
	RJMP _beep_ci

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x27:
	LDI  R26,LOW(50)
	RCALL SUBOPT_0x22
	RJMP SUBOPT_0x26


	.CSEG
_delay_ms:
	adiw r26,0
	breq __delay_ms1
__delay_ms0:
	__DELAY_USW 0x733
	wdr
	sbiw r26,1
	brne __delay_ms0
__delay_ms1:
	ret

__CPW02:
	CLR  R0
	CP   R0,R26
	CPC  R0,R27
	RET

__SAVELOCR6:
	ST   -Y,R21
__SAVELOCR5:
	ST   -Y,R20
__SAVELOCR4:
	ST   -Y,R19
__SAVELOCR3:
	ST   -Y,R18
__SAVELOCR2:
	ST   -Y,R17
	ST   -Y,R16
	RET

__LOADLOCR6:
	LDD  R21,Y+5
__LOADLOCR5:
	LDD  R20,Y+4
__LOADLOCR4:
	LDD  R19,Y+3
__LOADLOCR3:
	LDD  R18,Y+2
__LOADLOCR2:
	LDD  R17,Y+1
	LD   R16,Y
	RET

__LOADLOCR2P:
	LD   R16,Y+
	LD   R17,Y+
	RET

;END OF CODE MARKER
__END_OF_CODE:
