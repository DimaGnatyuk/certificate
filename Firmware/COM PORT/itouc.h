void int_to_uc (int number, unsigned char segment){

  int r1,r2,ost;
  unsigned char out;

  ost=0;
  while (number>9){
    number=number-10;
    ost++;
  }
  r1=ost;
  r2=number;

  switch (r1){
    case 0:  out=0x00;
             break;
    case 1:  out=0x10;
             break;
    case 2:  out=0x20;
             break;
    case 3:  out=0x30;
             break;
    case 4:  out=0x40;
             break;
    case 5:  out=0x50;
             break;
    case 6:  out=0x60;
             break;
    case 7:  out=0x70;
             break;
    case 8:  out=0x80;
             break;
    case 9:  out=0x90;
             break;
    default: out=0x00;
             break;
  }

  switch (r2){
    case 0:  out=out|0x00;
             break;
    case 1:  out=out|0x01;
             break;
    case 2:  out=out|0x02;
             break;
    case 3:  out=out|0x03;
             break;
    case 4:  out=out|0x04;
             break;
    case 5:  out=out|0x05;
             break;
    case 6:  out=out|0x06;
             break;
    case 7:  out=out|0x07;
             break;
    case 8:  out=out|0x08;
             break;
    case 9:  out=out|0x09;
             break;
    default: out=out|0x00;
             break;
  }

  *dotsi = 0b00100000;
  switch (segment){
    case 0x01:  *lefti = out;
                break;
    case 0x02:  *righti = out;
                break;
    default:    break;
 
  }
}