-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.25 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных secret_castle
CREATE DATABASE IF NOT EXISTS `secret_castle` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `secret_castle`;


-- Дамп структуры для таблица secret_castle.people
CREATE TABLE IF NOT EXISTS `people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `email` varchar(30) DEFAULT NULL,
  `has_md` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `has_md` (`has_md`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы secret_castle.people: ~11 rows (приблизительно)
DELETE FROM `people`;
/*!40000 ALTER TABLE `people` DISABLE KEYS */;
INSERT INTO `people` (`id`, `name`, `email`, `has_md`) VALUES
	(1, 'Test', 'test@mail.com', 'c164e15c1a9373199698070b45c116ad'),
	(2, 'Test 2', 'test1@mail.com', 'bce0fd84dc6813bfa24d3c0db1f2ad198'),
	(6, 'test', 'test@test', '50563c3bfdd6ad990031874f121993b7'),
	(13, 'Daim', 'Daim@ukr.net', '4887b3a8f5b7f9edb356fd98cf0d79fa'),
	(14, 'wer', 'wer@ukr.net', 'c3ba47e607c7dfa5a6b9017b9a1140b7'),
	(15, 'test', NULL, '250f4251d4eea3e7d75671623247e88e'),
	(16, 'test', NULL, 'b36e2f37458567deba8f18946bdf6167'),
	(18, 'test123@test', 'test123@test', 'f7b5905cabac516e4d0725cc34cdaa92'),
	(19, 'asdasd@ukr.net', 'asdasd@ukr.net', '366dd430fe57b87a3fa70f1f2fb68198'),
	(20, 'Daim@wer.com', 'Daim@wer.com', '0253a8f0817384601f9f90c7278d1d0c'),
	(21, 'test2@ukr.net', 'test2@ukr.net', '27eeaf60b32fcc04c9cde8e316640b2f');
/*!40000 ALTER TABLE `people` ENABLE KEYS */;


-- Дамп структуры для таблица secret_castle.reports
CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_reports_rooms` (`room_id`),
  KEY `FK_reports_states` (`state_id`),
  CONSTRAINT `FK_reports_rooms` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`),
  CONSTRAINT `FK_reports_states` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы secret_castle.reports: ~2 rows (приблизительно)
DELETE FROM `reports`;
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
INSERT INTO `reports` (`id`, `room_id`, `state_id`, `data`) VALUES
	(2, 15, -1, '2016-06-11 21:29:41'),
	(3, 14, 0, '2016-06-12 10:44:46');
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;


-- Дамп структуры для таблица secret_castle.rooms
CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `person_id_name` (`person_id`,`name`),
  CONSTRAINT `FK_rooms_peoples` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы secret_castle.rooms: ~9 rows (приблизительно)
DELETE FROM `rooms`;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` (`id`, `person_id`, `name`) VALUES
	(10, 1, 'Test 10'),
	(6, 1, 'Test 5'),
	(8, 1, 'Test 6'),
	(5, 1, 'Test 8'),
	(4, 2, 'Test Room 2'),
	(13, 19, 'dfgdfgFDGDFG'),
	(11, 19, 'Test'),
	(14, 21, 'Test'),
	(15, 21, 'іваіваіва');
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;


-- Дамп структуры для таблица secret_castle.states
CREATE TABLE IF NOT EXISTS `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `action` text NOT NULL,
  `isWarning` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы secret_castle.states: ~5 rows (приблизительно)
DELETE FROM `states`;
/*!40000 ALTER TABLE `states` DISABLE KEYS */;
INSERT INTO `states` (`id`, `name`, `action`, `isWarning`) VALUES
	(-3, 'Fatal Error', 'danger', 1),
	(-1, 'Error', 'danger', 1),
	(0, 'Closed', 'success', 0),
	(1, 'Open', 'info', 0),
	(5, 'Start', '', NULL);
/*!40000 ALTER TABLE `states` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
